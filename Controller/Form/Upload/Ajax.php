<?php

namespace GetNoticed\FormBuilder\Controller\Form\Upload;

use GetNoticed\FormBuilder\Service\FileUploaderService;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Exception\LocalizedException;


class Ajax extends Action
{
    /**
     * @var JsonFactory
     */
    private $resultJsonFactory;

    /**
     * @var FileUploaderService
     */
    private $fileUploaderService;

    public function __construct(
        Context $context,
        JsonFactory $resultJsonFactory,
        FileUploaderService $fileUploaderService
    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->fileUploaderService = $fileUploaderService;
    }


    public function execute()
    {
        if ($this->getRequest()->isAjax() === false) {
            throw new LocalizedException(__('Invalid request'));
        }

        try {
            $response = $this->fileUploaderService->processUpload();
        } catch (\Exception $e) {
            $response = [
                'error' => $e->getMessage()
            ];
        }

        $result = $this->resultJsonFactory->create();
        return $result->setData($response);
    }



}
