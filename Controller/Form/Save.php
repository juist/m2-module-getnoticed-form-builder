<?php
namespace GetNoticed\FormBuilder\Controller\Form;
use GetNoticed\FormBuilder as FB;
use Magento\Framework;
use Psr\Log;
use Magento\Framework\DataObject;
/**
 * @method Framework\App\Request\Http getRequest()
 */
class Save extends Framework\App\Action\Action
{
    /**
     * @var FB\Service\FormSessionPersistenceService
     */
    private $formSessionPersistenceService;
    /**
     * @var FB\Service\FormValidationService
     */
    private $formValidationService;
    /**
     * @var FB\Model\Data\FormFactory
     */
    private $formFactory;
    /**
     * @var Log\LoggerInterface
     */
    private $logger;
    public function __construct(
        Framework\App\Action\Context $context,
        FB\Service\FormSessionPersistenceService $formSessionPersistenceService,
        FB\Service\FormValidationService $formValidationService,
        FB\Model\Data\FormFactory $formFactory,
        Log\LoggerInterface $logger,
        dataObject $data
    ) {
        parent::__construct($context);
        $this->formSessionPersistenceService = $formSessionPersistenceService;
        $this->formValidationService = $formValidationService;
        $this->formFactory = $formFactory;
        $this->logger = $logger;
        $this->_data = $data;
    }

    public function execute()
    {
        // Gather form metadata
        $formCode = $this->getRequest()->getParam('form_code');
        // Gather form object itself
        try {
            $form = $this->formFactory->create(FB\Api\Data\FormInterface::class, ['form_code' => $formCode]);
        } catch (Framework\Exception\LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            return $this->_redirect($this->getRedirectUrl());
        }
        // Gather post values
        $post = $this->persistFormData($form, $formCode);
        // Validate the form
        try {
            $this->formValidationService->validateForm($form, $post);
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            return $this->_redirect($this->getReferrerUrl());
        }
        //set the form to the dataobject
        $this->setForm($form);
        $this->setPost($post);
        // Validation succeeded, perform actions
        foreach ($form->getActions() as $action) {
            try {
                $action->process($form, $post);
            } catch (Framework\Exception\LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                return $this->_redirect($this->getReferrerUrl());
            } catch (\Exception | \TypeError $e) {
                $this->messageManager->addErrorMessage(
                    __('An unknown error occurred while processing this form. Please try again later.')
                );
                $this->logger->error($e->getMessage());
                return $this->_redirect($this->getReferrerUrl());
            }
        }
        // On success, remove form data and send user back to redirect url
        $this->formSessionPersistenceService->removeFormData($formCode);
        if ($form->isSendSuccessMessage()) {
            $this->messageManager->addSuccessMessage(__('Your entry has been succesfully submitted.'));
        }
        return $this->_redirect($this->getRedirectUrl());
    }

    /**
     * @param \GetNoticed\FormBuilder\Api\Data\FormInterface $form
     *
     * @return array
     */
    public function getPostData(FB\Api\Data\FormInterface $form): array
    {
        return $this->getRequest()->getPostValue($form->getNameContainer()) ?: [];
    }

    /**
     * @param \GetNoticed\FormBuilder\Api\Data\FormInterface $form
     * @param string                                         $formCode
     *
     * @return array
     */
    protected function persistFormData(FB\Api\Data\FormInterface $form, string $formCode): array
    {
        $post = $this->getPostData($form);
        $this->formSessionPersistenceService->saveFormData($formCode, $post);
        return $post;
    }

    private function getReferrerUrl(): string
    {
        return (string)$this->getRequest()->getPostValue('_referrer_url');
    }

    private function getRedirectUrl(): string
    {
        return (string)$this->getRequest()->getPostValue('_redirect_url');
    }

    public function setForm(FB\Api\Data\FormInterface $form)
    {
        $this->_data->setData('form', $form);
    }

    public function setPost(array $post)
    {
        $this->_data->setData('post', $post);
    }
}
