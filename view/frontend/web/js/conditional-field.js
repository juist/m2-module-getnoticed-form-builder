define([
    'jquery',
    'mage/template'
], function ($, mageTemplate) {

    return function (configuration, element) {
        initialAction();

        var conditionalElement = document.getElementsByName(configuration.conditionalField.name)[0];

        // fix for jQuery Select2 elements not triggering native DOM events
        $("[name='"+configuration.conditionalField.name+"']").on("change", toggle.bind(element));

        conditionalElement.addEventListener('change', toggle.bind(element));

        var config = configuration;
        function toggle(event) {
            var condition = config.conditionValues.filter(
                validateFieldCondition(event.target.value)
            );

            if (condition.length > 0) {
                switch (condition[0].action) {
                    case 'hide':
                        element.closest("div.field").style.display = "none";
                        break;
                    case 'show':
                        element.closest("div.field").style.display = "block";
                        break;
                }
            } else {
                initialAction()
            }
        }

        function validateFieldCondition(fieldValue) {
            return function filter(condition) {
                return condition.value === fieldValue;
            };
        }

        function initialAction() {
            switch (configuration.initialAction) {
                case 'hide':
                    element.closest("div.field").style.display = "none";
                    break;
                case 'show':
                default:
                    element.closest("div.field").style.display = "block";
                    break;
            }

        }
    }
})
