define(
  ['jquery', 'GetNoticed_BAM/js/jquery.timepicker'],
  function ($) {
    return function(configuration, element) {

      var fieldId = configuration.time_id;
      var fromTimepickerEl = $('#'+ fieldId +'_from');
      var untilTimepickerEl = $('#' + fieldId +'_until');
      var resetTimeEl = $('#' + fieldId + '_reset');
      var MS_PER_MINUTE = 60000;
      var untilTimepickerContainerEl = $('#until_timepicker_contaier');
      var steps = 15;
      var timeSection = $('#time-selector-container');
      var timeToggle = $('#time-toggle');

      timeToggle.on("click", function (e) {
        e.preventDefault();
        timeSection.slideToggle();
        timeToggle.find("i").toggleClass("fa-caret-down fa-caret-up");
        fromTimepickerEl.val('')
        untilTimepickerEl.val('');
        fromTimepickerEl.timepicker(timepickerOptions);
        untilTimepickerEl.timepicker(timepickerOptions);
        untilTimepickerContainerEl.hide();
        resetTimeEl.hide();
      });

      var timepickerOptions = {
        timeFormat: 'HH:mm',
        startTime: new Date(0, 0, 0, 7, 0, 0),
        interval: steps
      }
      resetTimeEl.hide();
      $('input.timepicker').timepicker(timepickerOptions);

      fromTimepickerEl.timepicker('option', 'change', function (from_time) {
        if (from_time !== null && from_time !== false) {
          var untilTime = new Date(from_time.getTime() + steps * MS_PER_MINUTE);
          untilTimepickerEl.timepicker('option', 'minTime', untilTime);
          untilTimepickerContainerEl.fadeIn();
          resetTimeEl.fadeIn();
        }
      });

      resetTimeEl.on("click", function () {
        fromTimepickerEl.val('')
        untilTimepickerEl.val('');
        fromTimepickerEl.timepicker(timepickerOptions);
        untilTimepickerEl.timepicker(timepickerOptions);
        untilTimepickerContainerEl.hide();
        resetTimeEl.hide();
      });

    }
  });