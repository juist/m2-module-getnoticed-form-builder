define(['jquery', 'mage/template', 'mage/calendar'], function ($, mageTemplate) {
  return function (configuration, element) {

    $.datepicker.setDefaults({
      minDate: 1,
      prevText: '&#x3c;vorige', prevStatus: '',
      prevJumpText: '&#x3c;&#x3c;', prevJumpStatus: '',
      nextText: 'volgende&#x3e;', nextStatus: '',
      nextJumpText: '&#x3e;&#x3e;', nextJumpStatus: '',
      monthNames: ['Januari', 'Februari', 'Maart', 'April', 'Mei', 'Juni', 'Juli', 'Augustus', 'September', 'Oktober', 'November', 'December'],
      monthNamesShort: ['Jan', 'Feb', 'Maa', 'Apr', 'Mei', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dec'],
      dayNames: ['Zondag', 'Maandag', 'Dinsdag', 'Woensdag', 'Donderdag', 'Vrijdag', 'Zaterdag'],
      dayNamesShort: ['', 'Ma', 'Di', 'Wo', 'Do', 'Vr', 'Za'],
      dayNamesMin: ['Zo', 'Ma', 'Di', 'Wo', 'Do', 'Vr', 'Za'],
      showMonthAfterYear: false,
      changeMonth: true,
      changeYear: true,
      numberOfMonths: 1,
      dateFormat: "dd-mm-yy",
      altFormat: "yy-mm-dd"
    });

    var datepicker_field = "#" + configuration.date_field_id + '';
    var input_field = "#" + configuration.input_date_id;

    $(datepicker_field).datepicker({
      altField: input_field
    });

  }
})
