define(
    [
      'jquery','GetNoticed_FormBuilder/js/lib/select2'
    ],
    function($, select2) {
      return function(configuration, element) {
        var placeholder = (configuration.hasOwnProperty('placeholder')) ? configuration.placeholder : $.mage.__('Select a value...');
        var closeOnSelect = (configuration.hasOwnProperty('closeOnSelect')) ? configuration.closeOnSelect : true;
        var globalOptions = {
          closeOnSelect: closeOnSelect,
          placeholder: placeholder
        };

        var select = $(element).select2(globalOptions);
        select.on('select2:unselecting', function() {
          $(this).data('unselecting', true);
        }).on('select2:opening', function(e) {
          if ($(this).data('unselecting')) {
            $(this).removeData('unselecting');
            e.preventDefault();
          }
        });
        $(element).fadeIn();
      }
    }
);