<?php

namespace GetNoticed\FormBuilder\Helper\Config;

interface GeneralInterface
{
    public function isModuleEnabled(): bool;
}
