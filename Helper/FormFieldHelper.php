<?php

namespace GetNoticed\FormBuilder\Helper;

use Magento\Framework;
use GetNoticed\FormBuilder as FB;

class FormFieldHelper extends Framework\App\Helper\AbstractHelper
{
    public static function getFieldName(
        FB\Api\Data\FormInterface $form,
        FB\Api\Data\FieldInterface $field,
        ?string $nameSuffix = null
    ): string {
        return sprintf('%s[%s%s]', $form->getNameContainer(), $field->getName(), $nameSuffix);
    }

    public static function getFieldId(
        FB\Api\Data\FormInterface $form,
        FB\Api\Data\FieldInterface $field,
        ?string $nameSuffix = null
    ): string {
        return sprintf('%s_%s%s', $form->getFormId(), $field->getId(), $nameSuffix);
    }

    public static function getFieldLabel(
        FB\Api\Data\FieldInterface $field
    ): string {
        return __($field instanceof FB\Api\Data\Field\LabeledFieldInterface ? $field->getLabel() : $field->getName());
    }

    public static function isVisible(
        FB\Api\Data\FieldInterface $field
    ): bool {
        return $field instanceof FB\Api\Data\Field\VisibleFieldInterface;
    }
}
