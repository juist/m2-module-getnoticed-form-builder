<?php

namespace GetNoticed\FormBuilder\Model;

use Magento\Framework;
use GetNoticed\FormBuilder as FB;

/**
 * @method FB\Model\ResourceModel\Entry getResource()
 * @method FB\Model\ResourceModel\Entry\Collection getCollection()
 */
class Entry extends Framework\Model\AbstractModel implements
    FB\Api\Data\EntryInterface,
    Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'getnoticed_formbuilder_entry';

    protected $_cacheTag = 'getnoticed_formbuilder_entry';
    protected $_eventPrefix = 'getnoticed_formbuilder_entry';

    // Vars

    /**
     * @var FB\Api\Data\FormInterface
     */
    private $form;

    /**
     * @var array
     */
    private $entryData;

    protected function _construct()
    {
        $this->_init(FB\Model\ResourceModel\Entry::class);
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getForm(): FB\Api\Data\FormInterface
    {
        return $this->form;
    }

    public function setForm(FB\Api\Data\FormInterface $form): FB\Api\Data\EntryInterface
    {
        $this->form = $form;
        $this->setHasDataChanges(true);

        return $this;
    }

    public function getCreatedAt(): \DateTime
    {
        return new \DateTime($this->getData(self::FIELD_CREATED_AT));
    }

    public function setCreatedAt(\DateTime $createdAt): FB\Api\Data\EntryInterface
    {
        return $this->setData(self::FIELD_CREATED_AT, $createdAt->format('d-m-Y H:i:s'));
    }

    public function getEntryData(): array
    {
        return $this->entryData;
    }

    public function setEntryData(array $data): FB\Api\Data\EntryInterface
    {
        $this->entryData = $data;
        $this->setHasDataChanges(true);

        return $this;
    }
}
