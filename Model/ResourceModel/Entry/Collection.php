<?php

namespace GetNoticed\FormBuilder\Model\ResourceModel\Entry;

use Magento\Framework;
use GetNoticed\FormBuilder as FB;

class Collection extends Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = FB\Api\Data\EntryInterface::PRIMARY_KEY_FIELD;

    protected function _construct()
    {
        $this->_init(FB\Model\Entry::class, FB\Model\ResourceModel\Entry::class);
    }
}
