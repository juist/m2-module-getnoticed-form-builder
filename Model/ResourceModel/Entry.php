<?php

namespace GetNoticed\FormBuilder\Model\ResourceModel;

use Magento\Framework;
use GetNoticed\FormBuilder as FB;

class Entry extends Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init(FB\Api\Data\EntryInterface::TABLE_NAME, FB\Api\Data\EntryInterface::PRIMARY_KEY_FIELD);
    }
}
