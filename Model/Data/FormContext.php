<?php

namespace GetNoticed\FormBuilder\Model\Data;

use Magento\Framework;
use GetNoticed\FormBuilder as FB;

class FormContext
{
    /**
     * @var FB\Service\FormSessionPersistenceService
     */
    private $formSessionPersistenceService;

    /**
     * @var Framework\UrlInterface
     */
    private $url;

    public function __construct(
        FB\Service\FormSessionPersistenceService $formSessionPersistenceService,
        Framework\UrlInterface $url
    ) {
        $this->formSessionPersistenceService = $formSessionPersistenceService;
        $this->url = $url;
    }

    public function getFormSessionPersistenceService(): FB\Service\FormSessionPersistenceService
    {
        return $this->formSessionPersistenceService;
    }

    public function getUrl(): Framework\UrlInterface
    {
        return $this->url;
    }
}
