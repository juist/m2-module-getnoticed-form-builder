<?php

namespace GetNoticed\FormBuilder\Model\Data;

use Magento\Framework;
use GetNoticed\FormBuilder as FB;

class FormFactory implements Framework\ObjectManager\FactoryInterface
{
    /**
     * @var Framework\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @var Framework\App\ObjectManager\ConfigLoader
     */
    private $configLoader;

    public function __construct(
        Framework\ObjectManagerInterface $objectManager,
        Framework\App\ObjectManager\ConfigLoader $configLoader
    ) {
        $this->objectManager = $objectManager;
        $this->configLoader = $configLoader;
    }

    /**
     * @inheritDoc
     * @return object|FB\Api\Data\FormInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function create($requestedType, array $arguments = [])
    {
        if ($requestedType !== FB\Api\Data\FormInterface::class) {
            throw new Framework\Exception\LocalizedException(
                __('Can not create form: must extend "%1"', FB\Api\Data\FormInterface::class)
            );
        }

        // Load DI config and find object with form code = $arguments['form_code']
        $di = $this->configLoader->load(Framework\App\Area::AREA_GLOBAL);
        $requestedFormCode = $arguments['form_code'];

        if (empty($requestedFormCode)) {
            throw new Framework\Exception\LocalizedException(
                __('Please specify $arguments[form_code] to load a form.')
            );
        }

        foreach ($di as $className => $config) {
            if (array_key_exists('type', $config) !== true) {
                continue;
            }

            if ($this->isValidFormClass($config['type']) && $formCode = $this->getFormCodeFromConfig($config)) {
                if ($formCode === $requestedFormCode) {
                    return $this->objectManager->create($config['type'], $this->processArguments($config['arguments']));
                }
            }
        }

        throw new Framework\Exception\LocalizedException(
            __('Could not find a form with code: "%1"', $requestedFormCode)
        );
    }

    private function isValidFormClass(string $formClass): bool
    {
        return \is_subclass_of($formClass, FB\Api\Data\FormInterface::class)
            || \is_subclass_of($formClass, FB\Model\Data\AbstractForm::class);
    }

    private function getFormCodeFromConfig(array $config): ?string
    {
        return array_key_exists('formCode', $config['arguments']) && !empty($config['arguments']['formCode'])
            ? $config['arguments']['formCode'] : null;
    }

    /**
     * @inheritDoc
     */
    public function setObjectManager(Framework\ObjectManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    private function processArguments(array $arguments): array
    {
        foreach ($arguments as $key => $value) {
            $arguments[$key] = $this->processArgument($key, $value);
        }

        return $arguments;
    }

    private function processArgument(string $key, $argument)
    {
        if (is_array($argument)) {
            if (array_key_exists('instance', $argument)) {
                return $this->objectManager->create($argument['instance']);
            }

            return $this->processArguments($argument);
        }

        return $argument;
    }
}
