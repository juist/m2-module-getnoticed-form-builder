<?php

namespace GetNoticed\FormBuilder\Model\Data\FormFieldEntities;

use Magento\Customer;
use GetNoticed\FormBuilder as FB;

class CurrentCustomerEntity implements FB\Api\Data\FormFieldValueEntityInterface
{
    /**
     * @var Customer\Model\Customer
     */
    private $customer;

    // DI

    /**
     * @var Customer\Model\SessionFactory
     */
    private $sessionFactory;

    public function __construct(
        Customer\Model\SessionFactory $sessionFactory
    ) {
        $this->sessionFactory = $sessionFactory;
    }

    /**
     * @inheritDoc
     */
    public function getEntity()
    {
        if ($this->customer instanceof Customer\Model\Customer) {
            return $this->customer;
        }

        $session = $this->sessionFactory->create();

        if ($session->getCustomer()) {
            return $this->customer = $session->getCustomer();
        }

        return null;
    }
}
