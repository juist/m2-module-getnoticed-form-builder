<?php

namespace GetNoticed\FormBuilder\Model\Data;

use Magento\Framework;
use GetNoticed\FormBuilder as FB;

/**
 * Note: This class implements Framework\View\Element\Block\ArgumentInterface::class so it can be injected
 * through layout XML into the relevant blocks/view models that take care of rendering the form. Please do not remove!
 */
class AbstractForm implements FB\Api\Data\FormInterface, Framework\View\Element\Block\ArgumentInterface
{
    /**
     * @var string
     */
    private $formCode;

    /**
     * @var FB\Api\Data\FieldInterface[]
     */
    private $fields = [];

    /**
     * @var FB\Api\Data\ActionInterface[]
     */
    private $actions = [];

    /**
     * @var FB\Api\Data\ValidationInterface[]
     */
    private $customValidations = [];

    /**
     * @var array
     */
    private $config = [];

    /**
     * @var array
     */
    private $values = [];

    /**
     * @var string
     */
    private $redirectPath = '*/*/*';

    /**
     * @var array
     */
    private $redirectParams = [];

    /**
     * @var bool
     */
    private $isSendSuccessMessage = true;

    // DI

    /**
     * @var Framework\UrlInterface
     */
    private $url;

    public function __construct(
        FB\Model\Data\FormContext $context,
        string $formCode,
        array $fields = [],
        array $actions = [],
        array $customValidations = [],
        ?string $redirectPath = null,
        array $redirectParams = [],
        bool $isSendSuccessMessage = true
    ) {
        // Variables
        $this->formCode = $formCode;
        $this->fields = $fields;
        $this->actions = $actions;
        $this->customValidations = $customValidations;
        $this->values = $this->getDefaultValues();

        if (!empty($redirectPath)) {
            $this->redirectPath = $redirectPath;
        }

        $this->redirectParams = $redirectParams;
        $this->isSendSuccessMessage = $isSendSuccessMessage;

        // DI
        $this->url = $context->getUrl();
    }

    /**
     * @inheritDoc
     */
    public function getCode(): string
    {
        return $this->formCode;
    }

    /**
     * @inheritDoc
     */
    public function getFields(): array
    {
        return $this->fields;
    }

    /**
     * @inheritdoc
     */
    public function getFieldByPostName(string $postName): FB\Api\Data\FieldInterface
    {
        foreach ($this->fields as $field) {
            if ($field->getName() === $postName) {
                return $field;
            }
        }

        throw Framework\Exception\NoSuchEntityException::singleField('name', $postName);
    }

    /**
     * @inheritDoc
     */
    public function setFields(array $fields): FB\Api\Data\FormInterface
    {
        $this->fields = $fields;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getActions(): array
    {
        return $this->actions;
    }

    /**
     * @inheritDoc
     */
    public function setActions(array $actions): FB\Api\Data\FormInterface
    {
        $this->actions = $actions;

        return $this;
    }

    /**
     * @return FB\Api\Data\ValidationInterface[]
     */
    public function getCustomValidations(): array
    {
        return $this->customValidations;
    }

    /**
     * @inheritDoc
     */
    public function getConfig(): array
    {
        return $this->config;
    }

    /**
     * @inheritdoc
     */
    public function getConfigItem(string $key, $default = null)
    {
        return array_key_exists($key, $this->config) === true
            ? $this->config[$key] : $default;
    }

    /**
     * @inheritDoc
     */
    public function getNameContainer(): string
    {
        return $this->getConfigItem('name_container') ?: 'form_data';
    }

    /**
     * @inheritDoc
     */
    public function getFormId(): string
    {
        return $this->getConfigItem('form_id') ?: 'form';
    }

    /**
     * @inheritDoc
     */
    public function setConfig(array $config): FB\Api\Data\FormInterface
    {
        $this->config = $config;

        return $this;
    }

    /**
     * @param \GetNoticed\FormBuilder\Api\Data\FieldInterface      $field
     * @param \GetNoticed\FormBuilder\Api\Data\FieldInterface|null $parentField
     * @param int|null                                             $iteration
     *
     * @return string|int|float|double|array|null
     */
    public function getFieldValue(
        FB\Api\Data\FieldInterface $field,
        ?FB\Api\Data\FieldInterface $parentField = null,
        ?int $iteration = null
    ) {
        return array_key_exists($field->getName(), $this->values)
            ? $this->values[$field->getName()] : null;
    }

    /**
     * @inheritDoc
     */
    public function getValues(): array
    {
        return $this->values;
    }

    /**
     * @inheritDoc
     */
    public function setValues(array $values): FB\Api\Data\FormInterface
    {
        $this->values = $values;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getRedirectUrl(): string
    {
        return $this->url->getUrl($this->redirectPath, $this->redirectParams);
    }

    /**
     * @inheritDoc
     */
    public function isSendSuccessMessage(): bool
    {
        return $this->isSendSuccessMessage;
    }

    /**
     * Write plugins for this method if you have custom logic regarding a fields' default value.
     *
     * @return array
     */
    public function getDefaultValues(): array
    {
        return [];
    }

    public function getFileTypeFields(): array
    {
        $fileTypeFields = array_filter($this->getFields(), function (FB\Api\Data\FieldInterface $field) {
            return $field instanceof FB\Fields\FileType;
        });
        return $fileTypeFields;
    }

    public function setRedirectPath($path)
    {
        $this->redirectPath = $path;
        return $this;
    }
}
