<?php

namespace GetNoticed\FormBuilder\Model;

use GetNoticed\FormBuilder as FB;
use Magento\Framework\Exception\NoSuchEntityException;

class EntryRepository implements FB\Api\EntryRepositoryInterface
{
    /**
     * @var FB\Api\Data\EntryInterfaceFactory
     */
    private $factory;

    /**
     * @var FB\Model\ResourceModel\Entry
     */
    private $resource;

    public function __construct(
        FB\Api\Data\EntryInterfaceFactory $factory,
        FB\Model\ResourceModel\Entry $resource
    ) {
        $this->factory = $factory;
        $this->resource = $resource;
    }

    /**
     * @inheritdoc
     */
    public function get(int $entryId): FB\Api\Data\EntryInterface
    {
        $entry = $this->createEntry();
        $this->resource->load($entry, $entryId);

        if ($entry->getId() === null) {
            throw NoSuchEntityException::singleField('entry_id', $entryId);
        }

        return $entry;
    }

    /**
     * @inheritdoc
     *
     * @param FB\Api\Data\EntryInterface|FB\Model\Entry $entry
     */
    public function save(FB\Api\Data\EntryInterface $entry): FB\Api\Data\EntryInterface
    {
        $this->resource->save($entry);

        return $entry;
    }

    /**
     * @return FB\Api\Data\EntryInterface|FB\Model\Entry
     */
    private function createEntry(): FB\Api\Data\EntryInterface
    {
        return $this->factory->create();
    }
}
