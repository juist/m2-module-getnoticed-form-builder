<?php

namespace GetNoticed\FormBuilder\Action;

use Magento\Framework;
use Magento\Store;
use GetNoticed\Common;
use GetNoticed\FormBuilder as FB;

class SendEmail implements FB\Api\Data\ActionInterface
{
    const TEMPLATE_CODE = 'getnoticed_formbuilder_notification';

    /**
     * @var Common\Mail\TransportBuilderFactory
     */
    private $transportBuilderFactory;

    /**
     * @var Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var FB\Service\FormFieldValueProcessor
     */
    private $formFieldValueProcessor;

    /**
     * @var array
     */
    private $recipients = [];

    /**
     * @var string|null
     */
    private $emailTemplate = null;

    public function __construct(
        FB\Action\Context\SendMailContext $context,
        ?string $emailTemplate = null,
        array $recipients = []
    ) {
        $this->transportBuilderFactory = $context->getTransportBuilderFactory();
        $this->storeManager = $context->getStoreManager();
        $this->formFieldValueProcessor = $context->getFormFieldValueProcessor();
        $this->attachmentFactory = $context->getAttachmentFactory();
        $this->recipients = $recipients;

        if ($emailTemplate !== null) {
            $this->emailTemplate = $emailTemplate;
        }
    }

    /**
     * @inheritDoc
     */
    public function process(FB\Api\Data\FormInterface $form, array $post): void
    {
        $transportBuilder = $this->transportBuilderFactory->create();
        $transportBuilder->setTemplateIdentifier($this->getTemplate())
                         ->setTemplateOptions(
                             [
                                 'area'  => Framework\App\Area::AREA_FRONTEND,
                                 'store' => $this->storeManager->getStore()->getId()
                             ]
                         )
                         ->setFrom('general')
                         ->setTemplateVars($this->getTemplateVars($form, $post))
                         ->addTo('no-reply@bammaterieelverhuur.nl', 'BAM Materieel');

        foreach (array_keys($this->getRecipients()) as $email) {
            $transportBuilder->addBcc($email);
        }

        $this->addAttachments($form, $transportBuilder, $post);

        $transportBuilder->getTransport()->sendMessage();
    }

    /**
     * @param FB\Api\Data\FormInterface    $form
     * @param Common\Mail\TransportBuilder $transportBuilder
     * @param array                        $post
     */
    public function addAttachments(
        FB\Api\Data\FormInterface $form,
        Common\Mail\TransportBuilder $transportBuilder,
        array $post
    ): void {
        foreach ($form->getFileTypeFields() as $field) {
            /** @var $field FB\Api\Data\FieldInterface */
            if (!array_key_exists($field->getCode(), $post)) {
                continue;
            }
            foreach ($post[$field->getCode()] as $filePath) {
                $transportBuilder->addAttachment(
                    ...$this->attachmentFactory->createAttachment($filePath)
                );
            }
        }
    }

    public function getRecipients(): array
    {
        $recipients = [];

        foreach ($this->recipients as $recipient) {
            $recipients[$recipient['email']] = $recipient['name'];
        }

        return $recipients;
    }

    public function addRecipient(string $email, string $name): self
    {
        $this->recipients[] = ['email' => $email, 'name' => $name];

        return $this;
    }

    /**
     * @return Common\Mail\TransportBuilder
     */
    public function getTransportBuilder(): Common\Mail\TransportBuilder
    {
        return $this->transportBuilder;
    }

    /**
     * @return Store\Model\StoreManagerInterface
     */
    public function getStoreManager(): Store\Model\StoreManagerInterface
    {
        return $this->storeManager;
    }

    /**
     * If you want to send a different e-mail, modify this template.
     *
     * @return string
     */
    protected function getTemplate(): string
    {
        return $this->emailTemplate ?: self::TEMPLATE_CODE;
    }

    /**
     * @param \GetNoticed\FormBuilder\Api\Data\FormInterface $form
     * @param array                                          $post
     *
     * @return array
     */
    protected function getTemplateVars(FB\Api\Data\FormInterface $form, array $post): array
    {
        $baseData = array_merge(
            [
                'form' => $form,
                'post' => $post
            ]
        );

        $baseData = $this->filterFileTypeValues($form, $baseData);

        foreach ($this->formFieldValueProcessor->getEntities() as $entityKey => $formFieldValue) {
            $baseData[$entityKey] = $formFieldValue->getEntity();
        }

        return $baseData;
    }

    private function filterFileTypeValues(FB\Api\Data\FormInterface $form, array $baseData) {
        foreach ($form->getFileTypeFields() as $field) {
            /** @var $field FB\Fields\FileType */
            unset($baseData['post'][$field->getCode()]);
        }
        return $baseData;
    }
}
