<?php

namespace GetNoticed\FormBuilder\Action;

use Magento\Framework;
use GetNoticed\FormBuilder as FB;
use Psr\Log;

class CreateDbEntry implements FB\Api\Data\ActionInterface
{
    /**
     * @var FB\Api\EntryRepositoryInterface
     */
    private $entryRepository;

    /**
     * @var FB\Api\Data\EntryInterfaceFactory
     */
    private $entryFactory;

    /**
     * @var Log\LoggerInterface
     */
    private $logger;

    public function __construct(
        FB\Api\EntryRepositoryInterface $entryRepository,
        FB\Api\Data\EntryInterfaceFactory $entryFactory,
        Log\LoggerInterface $logger
    ) {
        $this->entryRepository = $entryRepository;
        $this->entryFactory = $entryFactory;
        $this->logger = $logger;
    }

    /**
     * @inheritDoc
     */
    public function process(
        FB\Api\Data\FormInterface $form,
        array $post
    ): void {
        $entry = $this->createEntry();
        $entry->setForm($form)->setEntryData($post);

        try {
            $this->entryRepository->save($entry);
        } catch (Framework\Exception\LocalizedException $e) {
            throw new Framework\Exception\LocalizedException(
                __('Unable to save form entry: %1', $e->getMessage())
            );
        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());

            throw new Framework\Exception\LocalizedException(
                __('Unable to save form entry: %1', 'error has been logged')
            );
        }
    }

    /**
     * @return FB\Api\Data\EntryInterface|FB\Model\Entry
     */
    private function createEntry(): FB\Api\Data\EntryInterface
    {
        return $this->entryFactory->create();
    }
}
