<?php

namespace GetNoticed\FormBuilder\Action\Context;

use Magento\Framework;
use Magento\Store;
use GetNoticed\Common;
use GetNoticed\FormBuilder as FB;

class SendMailContext
{
    /**
     * @var Common\Mail\TransportBuilderFactory
     */
    private $transportBuilderFactory;

    /**
     * @var Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var FB\Service\FormFieldValueProcessor
     */
    private $formFieldValueProcessor;

    public function __construct(
        Common\Mail\TransportBuilderFactory $transportBuilderFactory,
        Store\Model\StoreManagerInterface $storeManager,
        FB\Service\FormFieldValueProcessor $formFieldValueProcessor,
        FB\Service\AttachmentFactory $attachmentFactory
    ) {
        $this->transportBuilderFactory = $transportBuilderFactory;
        $this->storeManager = $storeManager;
        $this->formFieldValueProcessor = $formFieldValueProcessor;
        $this->attachmentFactory = $attachmentFactory;
    }

    /**
     * @return Common\Mail\TransportBuilderFactory
     */
    public function getTransportBuilderFactory(): Common\Mail\TransportBuilderFactory
    {
        return $this->transportBuilderFactory;
    }

    /**
     * @return Store\Model\StoreManagerInterface
     */
    public function getStoreManager(): Store\Model\StoreManagerInterface
    {
        return $this->storeManager;
    }

    /**
     * @return FB\Service\FormFieldValueProcessor
     */
    public function getFormFieldValueProcessor(): FB\Service\FormFieldValueProcessor
    {
        return $this->formFieldValueProcessor;
    }

    public function getAttachmentFactory(): FB\Service\AttachmentFactory
    {
        return $this->attachmentFactory;
    }
}
