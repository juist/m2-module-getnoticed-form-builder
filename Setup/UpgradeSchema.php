<?php

namespace GetNoticed\FormBuilder\Setup;

use Magento\Framework;
use Magento\Framework\DB\Ddl;
use GetNoticed\FormBuilder as FB;

class UpgradeSchema implements Framework\Setup\UpgradeSchemaInterface
{
    public function upgrade(
        Framework\Setup\SchemaSetupInterface $setup,
        Framework\Setup\ModuleContextInterface $context
    ) {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.1.0', '<')) {
            $this->createEntryTable($setup->getConnection());
        }

        $setup->endSetup();
    }

    /**
     * @param Framework\DB\Adapter\AdapterInterface $adapter
     *
     * @throws \Zend_Db_Exception
     */
    private function createEntryTable(
        Framework\DB\Adapter\AdapterInterface $adapter
    ): void {
        $adapter->createTable(
            $adapter->newTable(FB\Api\Data\EntryInterface::TABLE_NAME)
                    ->setComment('Form Builder Entries table')
                    ->addColumn(
                        FB\Api\Data\EntryInterface::PRIMARY_KEY_FIELD,
                        Ddl\Table::TYPE_INTEGER,
                        null,
                        [
                            'primary'  => true,
                            'unsigned' => true,
                            'identity' => true,
                            'nullable' => false
                        ],
                        'Entry ID'
                    )
                    ->addColumn(
                        FB\Api\Data\EntryInterface::FIELD_FORM,
                        Ddl\Table::TYPE_TEXT,
                        125,
                        [
                            'nullable' => false
                        ],
                        'Form Code'
                    )
                    ->addColumn(
                        FB\Api\Data\EntryInterface::FIELD_CREATED_AT,
                        Ddl\Table::TYPE_DATETIME,
                        null,
                        [
                            'nullable' => false
                        ],
                        'Created At'
                    )
                    ->addColumn(
                        FB\Api\Data\EntryInterface::FIELD_ENTRY_DATA,
                        Ddl\Table::TYPE_TEXT,
                        '64k',
                        [
                            'nullable' => false
                        ],
                        'Entry Data (json)'
                    )
        );
    }
}
