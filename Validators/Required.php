<?php

namespace GetNoticed\FormBuilder\Validators;

use GetNoticed\FormBuilder as FB;
use Magento\Framework;

class Required implements FB\Api\Data\FieldValidatorInterface
{
    /**
     * @var array
     */
    private $config = [];

    /**
     * @var FB\Service\ConditionService
     */
    private $conditionService;

    public function __construct(
        FB\Service\ConditionService $conditionService
    ) {
        $this->conditionService = $conditionService;
    }

    /**
     * @inheritDoc
     */
    public function validateField(
        FB\Api\Data\FormInterface $form,
        FB\Api\Data\FieldInterface $field,
        $value
    ): void {
        $fieldLabel = FB\Helper\FormFieldHelper::getFieldLabel($field);

        if ($this->conditionService->checkConditions($field, $value) === false ) {
            return;
        }

        if (empty($value) && ($value !== 0 || $value !== '0')) {
            throw new Framework\Exception\LocalizedException(
                __('%1 is a required field.', $fieldLabel)
            );
        }
    }

    public function getConfig(): array
    {
        return $this->config;
    }

    public function setConfig(array $config): FB\Api\Data\FieldValidatorInterface
    {
        $this->config = $config;

        return $this;
    }
}
