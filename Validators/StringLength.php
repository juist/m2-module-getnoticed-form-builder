<?php

namespace GetNoticed\FormBuilder\Validators;

use GetNoticed\FormBuilder as FB;
use Magento\Framework;

class StringLength implements FB\Api\Data\FieldValidatorInterface
{
    /**
     * @var array
     */
    private $config = [];

    /**
     * @inheritDoc
     */
    public function validateField(
        FB\Api\Data\FormInterface $form,
        FB\Api\Data\FieldInterface $field,
        $value
    ): void {
        $fieldLabel = FB\Helper\FormFieldHelper::getFieldLabel($field);

        $minLength = array_key_exists('min', $this->config) ? (int)$this->config['min'] : null;
        $maxLength = array_key_exists('max', $this->config) ? (int)$this->config['max'] : null;

        if ($minLength !== null && strlen($value) < $minLength) {
            throw new Framework\Exception\LocalizedException(
                __('%1 must contain at least %2 characters.', $fieldLabel, $minLength)
            );
        }

        if ($maxLength !== null && strlen($value) > $maxLength) {
            throw new Framework\Exception\LocalizedException(
                __('%1 must contain at most %2 characters.', $fieldLabel, $maxLength)
            );
        }
    }

    public function getConfig(): array
    {
        return $this->config;
    }

    public function setConfig(array $config): FB\Api\Data\FieldValidatorInterface
    {
        $this->config = $config;

        return $this;
    }
}
