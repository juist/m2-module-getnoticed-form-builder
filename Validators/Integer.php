<?php

namespace GetNoticed\FormBuilder\Validators;

use GetNoticed\FormBuilder as FB;
use Magento\Framework;

class Integer implements FB\Api\Data\FieldValidatorInterface
{
    /**
     * @var array
     */
    private $config = [];

    /**
     * @inheritDoc
     */
    public function validateField(
        FB\Api\Data\FormInterface $form,
        FB\Api\Data\FieldInterface $field,
        $value
    ): void {
        $fieldLabel = FB\Helper\FormFieldHelper::getFieldLabel($field);

        if (\is_numeric($value) !== true) {
            throw new Framework\Exception\LocalizedException(
                __('%1 must be a number.', $fieldLabel)
            );
        }

        $minValue = array_key_exists('min', $this->config) ? (int)$this->config['min'] : null;
        $maxValue = array_key_exists('max', $this->config) ? (int)$this->config['max'] : null;

        if ($minValue !== null && $value < $minValue) {
            throw new Framework\Exception\LocalizedException(
                __('%1 must have a value above %2.', $fieldLabel, $minValue)
            );
        }

        if ($maxValue !== null && $value > $maxValue) {
            throw new Framework\Exception\LocalizedException(
                __('%1 must have a value below %2.', $fieldLabel, $maxValue)
            );
        }
    }

    public function getConfig(): array
    {
        return $this->config;
    }

    public function setConfig(array $config): FB\Api\Data\FieldValidatorInterface
    {
        $this->config = $config;

        return $this;
    }
}
