<?php

namespace GetNoticed\FormBuilder\Fields;

use GetNoticed\FormBuilder\Api\Data\ConditionInterface;
use Magento\Framework\Exception\LocalizedException;

class Condition implements ConditionInterface
{

    public function __construct(
        $value,
        string $action
    ) {
        $this->value = $value;
        if (in_array($action, self::ALLOWED_ACTIONS) === false) {
            throw new LocalizedException(__('Invalid condition action'));
        }
        $this->action = $action;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function getAction(): string
    {

        return $this->action;
    }
}
