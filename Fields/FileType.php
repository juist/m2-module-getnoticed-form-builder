<?php

namespace GetNoticed\FormBuilder\Fields;

use GetNoticed\FormBuilder as FB;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Framework\UrlInterface;

class FileType implements
    FB\Api\Data\FieldInterface,
    FB\Api\Data\Field\VisibleFieldInterface,
    FB\Api\Data\Field\LabeledFieldInterface,
    FB\Api\Data\Field\DefaultValueFieldInterface,
    FB\Api\Data\Field\ValidatableFieldInterface,
    FB\Api\Data\Field\DisableAbleInterface,
    FB\Api\Data\Field\FileTypeFieldInterface
{
    /**
     * @var FB\Fields\TextType\BackendModel
     */
    private $backend;

    /**
     * @var FB\Fields\TextType\FrontendModel
     */
    private $frontend;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $label;

    /**
     * @var bool
     */
    private $disabled = false;

    /**
     * @var bool
     */
    private $readOnly = false;

    /**
     * @var string|null
     */
    private $default;

    /**
     * @var array
     */
    private $validators = [];

    /**
     * @var FB\Api\Data\ValidationInterface[]
     */
    private $jsValidations = [];

    /**
     * @var string
     */
    public $inputType = 'file';

    /**
     * @var FB\Api\Data\FieldInterface|null
     */
    private $parentField = null;

    /**
     * @var array
     */
    private $allowedExtensions = [];

    /**
     * @var string
     */
    private $uploadUrl;

    public function __construct(
        FB\Fields\FileType\BackendModelFactory $backendFactory,
        FB\Fields\FileType\FrontendModelFactory $frontendFactory,
        SerializerInterface $serializer,
        FB\JsValidation\AbstractValidatorFactory $validatorFactory,
        UrlInterface $urlBuilder,
        string $code,
        string $label,
        bool $disabled = false,
        bool $readOnly = false,
        ?string $default = null,
        array $validators = [],
        array $jsValidations = [],
        ?string $template = null,
        array $allowedExtensions = ['pdf'],
        string $uploadUrl = 'getnoticed/form_upload/ajax'
    ) {
        $this->backend = $backendFactory->create();
        $this->frontend = $frontendFactory->create();
        $this->serializer = $serializer;
        $this->validatorFactory = $validatorFactory;
        $this->urlBuilder = $urlBuilder;
        $this->code = $code;
        $this->label = $label;
        $this->disabled = $disabled;
        $this->readOnly = $readOnly;
        $this->default = $default;
        $this->validators = $validators;
        $this->jsValidations = $jsValidations;

        if ($template !== null) {
            $this->frontend->setParam('template', $template);
        }

        $this->allowedExtensions = $allowedExtensions;
        $this->uploadUrl = $uploadUrl;
    }

    /**
     * @inheritdoc
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @inheritdoc
     */
    public function getName(): string
    {
        return $this->getCode();
    }

    /**
     * @inheritdoc
     */
    public function getId(): string
    {
        return $this->getCode();
    }

    /**
     * @inheritdoc
     */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * @inheritdoc
     */
    public function getBackendModel(): FB\Api\Data\BackendModelInterface
    {
        return $this->backend;
    }

    /**
     * @inheritdoc
     */
    public function getFrontendModel(): FB\Api\Data\FrontendModelInterface
    {
        return $this->frontend;
    }

    /**
     * @return FB\Api\Data\FieldInterface|null
     */
    public function getParentField(): ?FB\Api\Data\FieldInterface
    {
        return $this->parentField;
    }

    /**
     * @param FB\Api\Data\FieldInterface|null $parentField
     *
     * @return $this
     */
    public function setParentField(?FB\Api\Data\FieldInterface $parentField): FB\Fields\TextType
    {
        $this->parentField = $parentField;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getDefault(): ?string
    {
        return $this->default;
    }

    public function getValidatorConfig(): array
    {
        return $this->validators;
    }

    /**
     * @return FB\Api\Data\ValidationInterface[]
     */
    public function getJsValidation(): array
    {
        $jsValidations = $this->jsValidations;
        if ($this->isRequired()) {
            $jsValidations[] = $this->validatorFactory->create([
                'validationClass' => 'required'
            ]);
        }
        return $jsValidations;
    }

    public function getJsValidationConfig(): string
    {
        $config = [];
        foreach ($this->getJsValidation() as $validation) {
            $config[$validation->getValidationClass()] = true;
        }
        return $this->serializer->serialize($config);
    }

    public function isRequired(): bool
    {
        return array_key_exists('required', $this->getValidatorConfig());
    }

    public function showRequiredMark(): bool
    {
        return $this->isRequired() === true
            && $this->isDisabled() === false
            && $this->isReadOnly() === false;
    }

    public function isDisabled(): bool
    {
        return $this->disabled;
    }

    public function isReadOnly(): bool
    {
        return $this->readOnly;
    }

    /**
     * @return array
     */
    public function getAllowedFileTypeExtensions(): array
    {
        return $this->allowedExtensions;
    }

    public function getUploadUrl(FB\Api\Data\FormInterface $form): string
    {
        return $this->urlBuilder->getUrl($this->uploadUrl);
    }

}
