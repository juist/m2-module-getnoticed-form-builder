<?php

namespace GetNoticed\FormBuilder\Fields\TextType;

use GetNoticed\FormBuilder as FB;

class FrontendModel extends FB\Fields\AbstractFrontendModel
{
    const CUSTOM_TEMPLATE_INPUT_TYPES = ['textarea', 'datepicker', 'timerangepicker'];

    /**
     * @var FB\Api\Data\FormInterface
     */
    private $form;

    /**
     * @var FB\Api\Data\FieldInterface|FB\Fields\TextType
     */
    private $field;

    /**
     * @var string
     */
    private $inputType = 'text';

    // DI

    /**
     * @var FB\Service\FormRenderer
     */
    private $formRenderer;

    /**
     * @inheritDoc
     */
    public function __construct(
        FB\Service\FormRenderer $formRenderer
    ) {
        $this->formRenderer = $formRenderer;
    }

    /**
     * @return string
     */
    public function getInputType(): string
    {
        return $this->inputType;
    }

    /**
     * @param string $inputType
     *
     * @return FB\Fields\TextType\FrontendModel
     */
    public function setInputType(string $inputType): FB\Fields\TextType\FrontendModel
    {
        $this->inputType = $inputType;

        return $this;
    }

    /**
     * @param FB\Api\Data\FormInterface                     $form
     * @param FB\Api\Data\FieldInterface|FB\Fields\TextType $field
     * @param array                                         $post
     *
     * @return array
     */
    public function getFieldValues(
        FB\Api\Data\FormInterface $form,
        FB\Api\Data\FieldInterface $field,
        array $post = []
    ): array {
        return [
            $field->getId() => [
                'label' => $field->getLabel(),
                'value' => $form->getFieldValue($field)
            ]
        ];
    }

    public function setFormField(FB\Api\Data\FormInterface $form, FB\Api\Data\FieldInterface $field)
    {
        $this->form = $form;
        $this->field = $field;
    }

    public function getRendererCode(): string
    {
        return in_array($this->inputType, self::CUSTOM_TEMPLATE_INPUT_TYPES)
            ? sprintf('%s.%s', 'text', $this->inputType) : 'text';
    }
}
