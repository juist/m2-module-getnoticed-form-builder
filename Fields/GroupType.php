<?php

namespace GetNoticed\FormBuilder\Fields;

use GetNoticed\FormBuilder as FB;
use Magento\Framework\Exception\NoSuchEntityException;

class GroupType implements
    FB\Api\Data\FieldInterface,
    FB\Api\Data\Field\VisibleFieldInterface,
    FB\Api\Data\Field\ValidatableFieldInterface
{
    /**
     * @var FB\Fields\GroupType\BackendModel
     */
    private $backend;

    /**
     * @var FB\Fields\GroupType\FrontendModel
     */
    private $frontend;

    /**
     * @var string
     */
    private $code;

    /**
     * @var FB\Api\Data\FieldInterface[]
     */
    private $fields = [];

    /**
     * @var string|null
     */
    private $label = null;

    /**
     * @var FB\Api\Data\FieldInterface|null
     */
    private $parentField = null;

    public function __construct(
        FB\Fields\GroupType\BackendModelFactory $backendFactory,
        FB\Fields\GroupType\FrontendModelFactory $frontendFactory,
        string $code,
        array $fields = [],
        ?string $label = null
    ) {
        $this->backend = $backendFactory->create();
        $this->frontend = $frontendFactory->create();
        $this->code = $code;
        $this->fields = $fields;
        $this->label = $label;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getName(): string
    {
        return $this->getCode();
    }

    public function getId(): string
    {
        return $this->getCode();
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    /**
     * @return FB\Api\Data\FieldInterface[]
     */
    public function getFields(): array
    {
        return $this->fields;
    }

    /**
     * @param string $code
     *
     * @return FB\Api\Data\FieldInterface
     * @throws Framework\Exception\NoSuchEntityException
     */
    public function getFieldByCode(string $code)
    {
        foreach ($this->fields as $field) {
            if ($field->getCode() === $code) {
                return $field;
            }
        }

        throw NoSuchEntityException::singleField('code', $code);
    }

    public function getBackendModel(): FB\Api\Data\BackendModelInterface
    {
        return $this->backend;
    }

    public function getFrontendModel(): FB\Api\Data\FrontendModelInterface
    {
        return $this->frontend;
    }

    /**
     * @return FB\Api\Data\FieldInterface|null
     */
    public function getParentField(): ?FB\Api\Data\FieldInterface
    {
        return $this->parentField;
    }

    /**
     * @param FB\Api\Data\FieldInterface|null $parentField
     *
     * @return $this
     */
    public function setParentField(?FB\Api\Data\FieldInterface $parentField): FB\Fields\GroupType
    {
        $this->parentField = $parentField;

        return $this;
    }

    public function getValidatorConfig(): array
    {
        return [];
    }

    public function isRequired(): bool
    {
        return array_key_exists('required', $this->getValidatorConfig());
    }

    public function showRequiredMark(): bool
    {
        return $this->isRequired() === true
            && $this->isDisabled() === false
            && $this->isReadOnly() === false;
    }
}
