<?php

namespace GetNoticed\FormBuilder\Fields;

use GetNoticed\FormBuilder as FB;

class TextAreaType extends FB\Fields\TextType
{
    public $inputType = 'textarea';
}
