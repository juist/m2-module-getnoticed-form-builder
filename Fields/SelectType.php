<?php

namespace GetNoticed\FormBuilder\Fields;

use Magento\Framework;
use GetNoticed\FormBuilder as FB;

class SelectType implements
    FB\Api\Data\FieldInterface,
    FB\Api\Data\Field\VisibleFieldInterface,
    FB\Api\Data\Field\LabeledFieldInterface,
    FB\Api\Data\Field\DefaultValueFieldInterface,
    FB\Api\Data\Field\ValidatableFieldInterface,
    FB\Api\Data\Field\DisableAbleInterface
{
    /**
     * @var FB\Fields\SelectType\BackendModel
     */
    private $backend;

    /**
     * @var FB\Fields\SelectType\FrontendModel
     */
    private $frontend;

    /**
     * @var FB\Api\Data\FieldInterface|null
     */
    private $parentField = null;

    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $label;

    /**
     * @var bool
     */
    private $multiSelect = false;

    /**
     * @var bool
     */
    private $disabled = false;

    /**
     * @var bool
     */
    private $readOnly = false;

    /**
     * @var FB\Api\Data\SelectOptionsInterface|null
     */
    private $options = null;

    /**
     * @var string|null
     */
    private $default;

    /**
     * @var bool
     */
    private $appendValueToLabel = false;

    /**
     * @var array
     */
    private $validators = [];

    public function __construct(
        FB\Fields\SelectType\BackendModelFactory $backendFactory,
        FB\Fields\SelectType\FrontendModelFactory $frontendFactory,
        string $code,
        string $label,
        bool $multiSelect = false,
        bool $disabled = false,
        bool $readOnly = false,
        ?FB\Api\Data\SelectOptionsInterface $options = null,
        ?string $default = null,
        bool $appendValueToLabel = false,
        array $validators = []
    ) {
        $this->backend = $backendFactory->create();
        $this->frontend = $frontendFactory->create();
        $this->code = $code;
        $this->label = $label;
        $this->multiSelect = $multiSelect;
        $this->disabled = $disabled;
        $this->readOnly = $readOnly;
        $this->options = $options;
        $this->default = $default;
        $this->appendValueToLabel = $appendValueToLabel;
        $this->validators = $validators;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getName(): string
    {
        return $this->getCode();
    }

    public function getId(): string
    {
        return $this->getCode();
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * @return bool
     */
    public function isAppendValueToLabel(): bool
    {
        return $this->appendValueToLabel;
    }

    /**
     * @return bool
     */
    public function isMultiSelect(): bool
    {
        return $this->multiSelect;
    }

    /**
     * @param bool $multiSelect
     *
     * @return FB\Fields\SelectType
     */
    public function setMultiSelect(bool $multiSelect): FB\Fields\SelectType
    {
        $this->multiSelect = $multiSelect;

        return $this;
    }

    public function getBackendModel(): FB\Api\Data\BackendModelInterface
    {
        return $this->backend;
    }

    public function getFrontendModel(): FB\Api\Data\FrontendModelInterface
    {
        return $this->frontend;
    }

    /**
     * @return FB\Api\Data\FieldInterface|null
     */
    public function getParentField(): ?FB\Api\Data\FieldInterface
    {
        return $this->parentField;
    }

    /**
     * @param FB\Api\Data\FieldInterface|null $parentField
     *
     * @return $this
     */
    public function setParentField(?FB\Api\Data\FieldInterface $parentField): FB\Fields\SelectType
    {
        $this->parentField = $parentField;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getDefault(): ?string
    {
        return $this->default;
    }

    public function getValidatorConfig(): array
    {
        return $this->validators;
    }

    public function isRequired(): bool
    {
        return array_key_exists('required', $this->getValidatorConfig());
    }

    public function showRequiredMark(): bool
    {
        return $this->isRequired() === true
            && $this->isDisabled() === false
            && $this->isReadOnly() === false;
    }

    public function isDisabled(): bool
    {
        return $this->disabled;
    }

    public function isReadOnly(): bool
    {
        return $this->readOnly;
    }

    public function setOptionsEntities(
        FB\Api\Data\FormInterface $form,
        FB\Api\Data\FieldInterface $field,
        ?FB\Api\Data\FieldInterface $parentField = null,
        ?int $iteration = null
    ): void {
        if ($this->options instanceof FB\Api\Data\SelectOptionsInterface) {
            $this->options->setEntities($form, $field, $parentField, $iteration);

            $options = $this->options->toOptionArray();

            if (empty($options) || is_array($options) !== true || count($options) < 1) {
                $this->disabled = true;
            } else {
                $this->disabled = false;
            }
        } else {
            $this->disabled = true;
        }
    }

    public function hasOptions(): bool
    {
        if ($this->options instanceof FB\Api\Data\SelectOptionsInterface) {
            return !empty($this->options->toOptionArray());
        }

        return false;
    }

    public function getOptions(): array
    {
        if ($this->options instanceof FB\Api\Data\SelectOptionsInterface) {
            return $this->options->toOptionArray();
        }

        return [];
    }

    /**
     * @param string $value
     *
     * @return string
     * @throws Framework\Exception\NoSuchEntityException
     */
    public function getOptionLabel(string $value): string
    {
        foreach ($this->getOptions() as $option) {
            if ($option['value'] === $value) {
                return $option['label'];
            }
        }

        throw new Framework\Exception\NoSuchEntityException(
            __('Can not find option label for value "%1" in field "%2"', $value, $this->getName())
        );
    }
}
