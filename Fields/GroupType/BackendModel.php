<?php

namespace GetNoticed\FormBuilder\Fields\GroupType;

use Magento\Framework;
use GetNoticed\FormBuilder as FB;

class BackendModel implements FB\Api\Data\BackendModelInterface
{
    /**
     * @var FB\Api\Data\FieldValidatorInterface[]
     */
    private $validators = [];

    // DI

    /**
     * @var FB\Service\FormValidationService
     */
    private $formValidationService;

    public function __construct(
        FB\Service\FormValidationService $formValidationService
    ) {
        $this->formValidationService = $formValidationService;
    }

    public function addValidators(FB\Api\Data\FieldValidatorInterface ...$validators): void
    {
        $this->validators = $validators;
    }

    /**
     * @inheritDoc
     */
    public function validate(
        FB\Api\Data\FormInterface $form,
        FB\Api\Data\FieldInterface $field,
        $value
    ): void {
        foreach ($this->validators as $validator) {
            $validator->validateField($form, $field, $value);
        }

        foreach ($field->getFields() as $groupField) {
            $this->formValidationService->validateField($form, $groupField, $value);
        }
    }
}
