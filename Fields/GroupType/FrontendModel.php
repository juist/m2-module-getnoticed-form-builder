<?php

namespace GetNoticed\FormBuilder\Fields\GroupType;

use GetNoticed\FormBuilder as FB;

class FrontendModel extends FB\Fields\AbstractFrontendModel
{
    /**
     * @var FB\Api\Data\FormInterface
     */
    private $form;

    /**
     * @var FB\Api\Data\FieldInterface|FB\Fields\GroupType
     */
    private $field;

    /**
     * @var FB\Api\FormRendererInterface
     */
    private $formRenderer;

    public function __construct(
        FB\Api\FormRendererInterface $formRenderer
    ) {
        $this->formRenderer = $formRenderer;
    }

    public function setFormField(FB\Api\Data\FormInterface $form, FB\Api\Data\FieldInterface $field)
    {
        $this->form = $form;
        $this->field = $field;
    }

    public function getRendererCode(): string
    {
        return 'group';
    }

    public function renderFieldHtml(
        FB\Api\Data\FieldInterface $field
    ): string {
        $field->getFrontendModel()->setFormField($this->form, $field);
        $renderer = $this->formRenderer->getFieldsRendererList()
                                       ->getRenderer($field->getFrontendModel()->getRendererCode());
        $renderer->setData('form', $this->form)->setData('field', $field);

        return $renderer->toHtml();
    }

    /**
     * @param FB\Api\Data\FormInterface                      $form
     * @param FB\Api\Data\FieldInterface|FB\Fields\GroupType $field
     * @param array                                          $post
     *
     * @return array
     */
    public function getFieldValues(
        FB\Api\Data\FormInterface $form,
        FB\Api\Data\FieldInterface $field,
        array $post = []
    ): array {
        $values = [];

        $values[$field->getCode()] = [
            'label' => $field->getLabel() ?: '',
            'value' => []
        ];
        foreach ($field->getFields() as $groupField) {
            $values[$field->getCode()]['value'][] = [
                'label' => FB\Helper\FormFieldHelper::getFieldLabel($groupField),
                'value' => $post[$groupField->getName()] ?: $form->getFieldValue($groupField)
            ];
        }

        return $values;
    }
}
