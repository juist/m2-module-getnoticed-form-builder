<?php

namespace GetNoticed\FormBuilder\Fields;

use GetNoticed\FormBuilder as FB;

class EmailType extends FB\Fields\TextType
{
    public $inputType = 'email';
}
