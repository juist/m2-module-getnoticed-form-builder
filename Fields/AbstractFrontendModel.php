<?php

namespace GetNoticed\FormBuilder\Fields;

use GetNoticed\FormBuilder\Api\Data\FrontendModelInterface;

abstract class AbstractFrontendModel implements FrontendModelInterface
{
    /**
     * @var array
     */
    protected $params = [];

    public function getParams(): array
    {
        return $this->params;
    }

    public function getParam(string $key, $default = null)
    {
        return array_key_exists($key, $this->params) ? $this->params[$key] : $default;
    }

    public function setParams(array $params): FrontendModelInterface
    {
        $this->params = $params;

        return $this;
    }

    public function setParam(string $key, $value): FrontendModelInterface
    {
        $this->params[$key] = $value;

        return $this;
    }

    public function unsetParam(string $key): FrontendModelInterface
    {
        unset($this->params[$key]);

        return $this;
    }
}
