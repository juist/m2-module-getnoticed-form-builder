<?php

namespace GetNoticed\FormBuilder\Fields\ButtonType;

use Magento\Framework;
use GetNoticed\FormBuilder as FB;

class BackendModel implements FB\Api\Data\BackendModelInterface
{
    /**
     * @inheritDoc
     */
    public function addValidators(FB\Api\Data\FieldValidatorInterface ...$validators): void
    {
        return;
    }

    /**
     * @inheritDoc
     */
    public function validate(
        FB\Api\Data\FormInterface $form,
        FB\Api\Data\FieldInterface $field,
        $value
    ): void {
        return;
    }
}
