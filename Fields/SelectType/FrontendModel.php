<?php

namespace GetNoticed\FormBuilder\Fields\SelectType;

use GetNoticed\FormBuilder as FB;

class FrontendModel extends FB\Fields\AbstractFrontendModel
{
    /**
     * @var FB\Api\Data\FormInterface
     */
    private $form;

    /**
     * @var FB\Api\Data\FieldInterface|FB\Fields\TextType
     */
    private $field;

    /**
     * @param FB\Api\Data\FormInterface                       $form
     * @param FB\Api\Data\FieldInterface|FB\Fields\SelectType $field
     * @param array                                           $post
     *
     * @return array
     */
    public function getFieldValues(
        FB\Api\Data\FormInterface $form,
        FB\Api\Data\FieldInterface $field,
        array $post = []
    ): array {
        if ($field->isMultiSelect() && is_array($form->getFieldValue($field))) {
            $values = [];

            foreach ($form->getFieldValue($field) as $value) {
                $values[$field->getId()][] = [
                    'label' => $field->getLabel(),
                    'value' => $value
                ];
            }

            return $values;
        } elseif ($field->isMultiSelect() === false) {
            return [
                $field->getId() => [
                    'label' => $field->getLabel(),
                    'value' => $form->getFieldValue($field)
                ]
            ];
        } else {
            return [];
        }
    }

    public function setFormField(FB\Api\Data\FormInterface $form, FB\Api\Data\FieldInterface $field)
    {
        $this->form = $form;
        $this->field = $field;
    }

    public function getRendererCode(): string
    {
        return 'select';
    }

    public function isRequired(FB\Api\Data\FieldInterface $field): bool
    {
        if (!$field instanceof FB\Api\Data\Field\ValidatableFieldInterface) {
            return false;
        }

        return array_key_exists('required', $field->getValidatorConfig());
    }
}
