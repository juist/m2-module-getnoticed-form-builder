<?php

namespace GetNoticed\FormBuilder\Fields;

use GetNoticed\FormBuilder as FB;

class NumberType extends FB\Fields\TextType
{
    public $inputType = 'number';
}
