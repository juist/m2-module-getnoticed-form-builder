<?php

namespace GetNoticed\FormBuilder\Fields\HtmlType;

use Magento\Framework;
use GetNoticed\FormBuilder as FB;

class BackendModel implements FB\Api\Data\BackendModelInterface
{
    public function addValidators(FB\Api\Data\FieldValidatorInterface ...$validators): void
    {
        // No validation
    }

    /**
     * @inheritDoc
     */
    public function validate(
        FB\Api\Data\FormInterface $form,
        FB\Api\Data\FieldInterface $field,
        $value
    ): void {
        // No validation
    }
}
