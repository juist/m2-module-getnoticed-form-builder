<?php

namespace GetNoticed\FormBuilder\Fields;

use GetNoticed\FormBuilder as FB;

class CheckboxType implements
    FB\Api\Data\FieldInterface,
    FB\Api\Data\Field\VisibleFieldInterface,
    FB\Api\Data\Field\LabeledFieldInterface,
    FB\Api\Data\Field\DefaultValueFieldInterface,
    FB\Api\Data\Field\ValidatableFieldInterface,
    FB\Api\Data\Field\DisableAbleInterface
{
    /**
     * @var FB\Fields\CheckboxType\BackendModel
     */
    private $backend;

    /**
     * @var FB\Fields\CheckboxType\FrontendModel
     */
    private $frontend;

    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $label;

    /**
     * @var bool
     */
    private $disabled = false;

    /**
     * @var bool
     */
    private $readOnly = false;

    /**
     * @var string|null
     */
    private $default;

    /**
     * @var array
     */
    private $validators = [];

    /**
     * @var FB\Api\Data\FieldInterface|null
     */
    private $parentField = null;

    public function __construct(
        FB\Fields\CheckboxType\BackendModelFactory $backendFactory,
        FB\Fields\CheckboxType\FrontendModelFactory $frontendFactory,
        string $code,
        string $label,
        bool $disabled = false,
        bool $readOnly = false,
        ?string $default = null,
        array $validators = []
    ) {
        $this->backend = $backendFactory->create();
        $this->frontend = $frontendFactory->create();
        $this->code = $code;
        $this->label = $label;
        $this->disabled = $disabled;
        $this->readOnly = $readOnly;
        $this->default = $default;
        $this->validators = $validators;
    }

    /**
     * @inheritdoc
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @inheritdoc
     */
    public function getName(): string
    {
        return $this->getCode();
    }

    /**
     * @inheritdoc
     */
    public function getId(): string
    {
        return $this->getCode();
    }

    /**
     * @inheritdoc
     */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * @inheritdoc
     */
    public function getBackendModel(): FB\Api\Data\BackendModelInterface
    {
        return $this->backend;
    }

    /**
     * @inheritdoc
     */
    public function getFrontendModel(): FB\Api\Data\FrontendModelInterface
    {
        return $this->frontend;
    }

    /**
     * @return FB\Api\Data\FieldInterface|null
     */
    public function getParentField(): ?FB\Api\Data\FieldInterface
    {
        return $this->parentField;
    }

    /**
     * @param FB\Api\Data\FieldInterface|null $parentField
     *
     * @return $this
     */
    public function setParentField(?FB\Api\Data\FieldInterface $parentField): FB\Fields\CheckboxType
    {
        $this->parentField = $parentField;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getDefault(): ?string
    {
        return $this->default;
    }

    public function getValidatorConfig(): array
    {
        return $this->validators;
    }

    public function isRequired(): bool
    {
        return array_key_exists('required', $this->getValidatorConfig());
    }

    public function showRequiredMark(): bool
    {
        return $this->isRequired() === true
            && $this->isDisabled() === false
            && $this->isReadOnly() === false;
    }

    public function isDisabled(): bool
    {
        return $this->disabled;
    }

    public function isReadOnly(): bool
    {
        return $this->readOnly;
    }
}
