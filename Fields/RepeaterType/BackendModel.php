<?php

namespace GetNoticed\FormBuilder\Fields\RepeaterType;

use Magento\Framework;
use GetNoticed\FormBuilder as FB;

class BackendModel implements FB\Api\Data\BackendModelInterface
{
    /**
     * @var FB\Service\FormValidationService
     */
    private $formValidationService;

    public function __construct(
        FB\Service\FormValidationService $formValidationService
    ) {
        $this->formValidationService = $formValidationService;
    }

    /**
     * @inheritDoc
     */
    public function addValidators(FB\Api\Data\FieldValidatorInterface ...$validators): void
    {
        return;
    }

    /**
     * @inheritDoc
     *
     * @param FB\Api\Data\FormInterface                         $form
     * @param FB\Api\Data\FieldInterface|FB\Fields\RepeaterType $field
     *
     * @throws Framework\Exception\LocalizedException
     */
    public function validate(
        FB\Api\Data\FormInterface $form,
        FB\Api\Data\FieldInterface $field,
        $value
    ): void {
        $numberOfValues = count($value);

        foreach ($field->getFields() as $repeaterField) {
            if(is_array($repeaterField)) {
                $repeaterField =  $repeaterField['type'];
            }
            for ($i = 0; $i < $numberOfValues; $i++) {
                $this->formValidationService->validateField(
                    $form,
                    $repeaterField,
                    $form->getFieldValue($repeaterField, $field, $i)
                        ?: (array_key_exists($i, $value) && array_key_exists($repeaterField->getName(), $value[$i])
                        ? $value[$i][$repeaterField->getName()] : '')
                );
            }
        }
    }
}
