<?php

namespace GetNoticed\FormBuilder\Fields\RepeaterType;

use GetNoticed\FormBuilder as FB;

class FrontendModel extends FB\Fields\AbstractFrontendModel
{
    /**
     * @var FB\Api\Data\FormInterface
     */
    private $form;

    /**
     * @var FB\Api\Data\FieldInterface|FB\Fields\GroupType
     */
    private $field;

    /**
     * @var FB\Api\FormRendererInterface
     */
    private $formRenderer;

    public function __construct(
        FB\Api\FormRendererInterface $formRenderer
    ) {
        $this->formRenderer = $formRenderer;
    }

    public function setFormField(FB\Api\Data\FormInterface $form, FB\Api\Data\FieldInterface $field)
    {
        $this->form = $form;
        $this->field = $field;
    }

    public function getRendererCode(): string
    {
        return 'repeater';
    }

    public function renderFieldHtml(
        FB\Api\Data\FieldInterface $field,
        array $fieldParams = []
    ): string {
        /** @var FB\Fields\TextType\FrontendModel $frontend */
        $frontend = $field->getFrontendModel();
        $frontend->setFormField($this->form, $field);
        $frontend->setParams($fieldParams);

        $renderer = $this->formRenderer->getFieldsRendererList()->getRenderer($frontend->getRendererCode());
        $renderer->setData('form', $this->form)->setData('field', $field);

        return $renderer->toHtml();
    }

    /**
     * @param FB\Api\Data\FieldInterface|FB\Fields\RepeaterType $field
     *
     * @return array
     */
    public function getEmptyValue(
        FB\Api\Data\FieldInterface $field
    ): array {
        $emptyValue = [];

        foreach ($field->getFields() as $loopField) {
            $emptyValue[$loopField->getName()] = '';
        }

        return $emptyValue;
    }

    /**
     * @param FB\Api\Data\FormInterface                         $form
     * @param FB\Api\Data\FieldInterface|FB\Fields\RepeaterType $field
     * @param array                                             $post
     *
     * @return array
     */
    public function getFieldValues(
        FB\Api\Data\FormInterface $form,
        FB\Api\Data\FieldInterface $field,
        array $post = []
    ): array {
//        $values = [];
//
//        $values[$field->getCode()] = [
//            'label' => $field->getLabel() ?: '',
//            'value' => []
//        ];
//        foreach ($field->getFields() as $groupField) {
//            $values[$field->getCode()]['value'][] = [
//                'label' => FB\Helper\FormFieldHelper::getFieldLabel($groupField),
//                'value' => $post[$groupField->getName()] ?: $form->getFieldValue($groupField)
//            ];
//        }
//
//        return $values;
        ///////////

        $values = [];


        foreach ($post[$field->getName()] as $iteration => $data) {
            $value = [];

            foreach ($field->getFields() as $loopField) {
                if(is_array($loopField)) {
                    $loopField =  $loopField['type'];
                }
                $key = FB\Helper\FormFieldHelper::getFieldId($form, $loopField);
                $values[$iteration]['label'] = '';
                $values[$iteration]['value'][$key] = [
                    'label' => FB\Helper\FormFieldHelper::getFieldLabel($loopField),
                    'value' => []
                ];

                if (array_key_exists($loopField->getName(), $data) && !empty($data[$loopField->getName()])) {
                    $values[$iteration]['value'][$key]['value'] = $data[$loopField->getName()];
                } else {
                    $values[$iteration]['value'][$key]['value'] = $form->getFieldValue($loopField, $field, $iteration);
                }
            }
        }

        return $values;
    }
}
