<?php

namespace GetNoticed\FormBuilder\Fields;

use Magento\Framework;
use GetNoticed\FormBuilder as FB;
use Magento\Framework\Serialize\SerializerInterface;

class RadioType implements
    FB\Api\Data\FieldInterface,
    FB\Api\Data\Field\VisibleFieldInterface,
    FB\Api\Data\Field\LabeledFieldInterface,
    FB\Api\Data\Field\DefaultValueFieldInterface,
    FB\Api\Data\Field\ValidatableFieldInterface,
    FB\Api\Data\Field\DisableAbleInterface
{
    /**
     * @var FB\Fields\RadioType\BackendModel
     */
    private $backend;

    /**
     * @var FB\Fields\RadioType\FrontendModel
     */
    private $frontend;

    /**
     * @var FB\JsValidation\AbstractValidatorFactory
     */
    private $validatorFactory;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var FB\Api\Data\FieldInterface|null
     */
    private $parentField = null;

    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $label;

    /**
     * @var bool
     */
    private $multiSelect = false;

    /**
     * @var bool
     */
    private $disabled = false;

    /**
     * @var bool
     */
    private $readOnly = false;

    /**
     * @var FB\Api\Data\SelectOptionsInterface|null
     */
    private $options = null;

    /**
     * @var string|null
     */
    private $default;

    /**
     * @var bool
     */
    private $appendValueToLabel = false;

    /**
     * @var array
     */
    private $validators = [];

    /**
     * @var FB\Api\Data\ValidationInterface[]
     */
    private $jsValidations = [];

    public function __construct(
        FB\Fields\RadioType\BackendModelFactory $backendFactory,
        FB\Fields\RadioType\FrontendModelFactory $frontendFactory,
        SerializerInterface $serializer,
        FB\JsValidation\AbstractValidatorFactory $validatorFactory,
        string $code,
        string $label,
        bool $disabled = false,
        bool $readOnly = false,
        Framework\Data\OptionSourceInterface $options = null,
        ?string $default = null,
        bool $appendValueToLabel = false,
        array $validators = [],
        array $jsValidations = []
    ) {
        $this->backend = $backendFactory->create();
        $this->frontend = $frontendFactory->create();
        $this->serializer = $serializer;
        $this->validatorFactory = $validatorFactory;
        $this->code = $code;
        $this->label = $label;
        $this->disabled = $disabled;
        $this->readOnly = $readOnly;
        $this->options = $options;
        $this->default = $default;
        $this->appendValueToLabel = $appendValueToLabel;
        $this->validators = $validators;
        $this->jsValidations = $jsValidations;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getName(): string
    {
        return $this->getCode();
    }

    public function getId(): string
    {
        return $this->getCode();
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * @return bool
     */
    public function isAppendValueToLabel(): bool
    {
        return $this->appendValueToLabel;
    }

    public function getBackendModel(): FB\Api\Data\BackendModelInterface
    {
        return $this->backend;
    }

    public function getFrontendModel(): FB\Api\Data\FrontendModelInterface
    {
        return $this->frontend;
    }

    /**
     * @return FB\Api\Data\FieldInterface|null
     */
    public function getParentField(): ?FB\Api\Data\FieldInterface
    {
        return $this->parentField;
    }

    /**
     * @param FB\Api\Data\FieldInterface|null $parentField
     *
     * @return $this
     */
    public function setParentField(?FB\Api\Data\FieldInterface $parentField): FB\Fields\SelectType
    {
        $this->parentField = $parentField;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getDefault(): ?string
    {
        return $this->default;
    }

    public function getValidatorConfig(): array
    {
        return $this->validators;
    }

    /**
     * @return FB\Api\Data\ValidationInterface[]
     */
    public function getJsValidation(): array
    {
        $jsValidations = $this->jsValidations;
        if ($this->isRequired()) {
            $jsValidations[] = $this->validatorFactory->create([
                'validationClass' => 'required'
            ]);
        }
        return $jsValidations;
    }

    public function getJsValidationConfig(): string
    {
        $config = [];
        foreach ($this->getJsValidation() as $validation) {
            $config[$validation->getValidationClass()] = true;
        }
        return $this->serializer->serialize($config);
    }

    public function isRequired(): bool
    {
        return array_key_exists('required', $this->getValidatorConfig());
    }

    public function showRequiredMark(): bool
    {
        return $this->isRequired() === true
            && $this->isDisabled() === false
            && $this->isReadOnly() === false;
    }

    public function isDisabled(): bool
    {
        return $this->disabled;
    }

    public function isReadOnly(): bool
    {
        return $this->readOnly;
    }

    public function hasOptions(): bool
    {
        return !empty($this->options->toOptionArray());
    }

    public function getOptions(): array
    {
        return $this->options->toOptionArray();
    }

    /**
     * @param string $value
     *
     * @return string
     * @throws Framework\Exception\NoSuchEntityException
     */
    public function getOptionLabel(string $value): string
    {
        foreach ($this->getOptions() as $option) {
            if ($option['value'] === $value) {
                return $option['label'];
            }
        }

        throw new Framework\Exception\NoSuchEntityException(
            __('Can not find option label for value "%1" in field "%2"', $value, $this->getName())
        );
    }
}
