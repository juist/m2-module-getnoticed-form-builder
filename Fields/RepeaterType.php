<?php

namespace GetNoticed\FormBuilder\Fields;

use Magento\Framework;
use GetNoticed\FormBuilder as FB;

class RepeaterType implements
    FB\Api\Data\FieldInterface,
    FB\Api\Data\Field\ValidatableFieldInterface,
    FB\Api\Data\Field\VisibleFieldInterface
{
    /**
     * @var FB\Fields\RepeaterType\BackendModel
     */
    private $backend;

    /**
     * @var FB\Fields\RepeaterType\FrontendModel
     */
    private $frontend;

    /**
     * @var FB\Api\Data\FieldInterface|null
     */
    private $parentField = null;

    /**
     * @var string
     */
    private $code;

    /**
     * @var FB\Api\Data\FieldInterface[]
     */
    private $fields = [];

    /**
     * @var int|null
     */
    private $activeIteration = null;

    public function __construct(
        FB\Fields\RepeaterType\BackendModelFactory $backendFactory,
        FB\Fields\RepeaterType\FrontendModelFactory $frontendFactory,
        string $code,
        array $fields = []
    ) {
        $this->backend = $backendFactory->create();
        $this->frontend = $frontendFactory->create();
        $this->code = $code;
        $this->fields = $fields;

        foreach ($this->fields as $field) {
            if(is_array($field)) {
                $field =  $field['type'];
            }
            $field->setParentField($this);
        }
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getName(): string
    {
        return $this->getCode();
    }

    public function getId(): string
    {
        return $this->getCode();
    }

    /**
     * @return FB\Api\Data\FieldInterface[]
     */
    public function getFields(): array
    {
        return $this->fields;
    }

    /**
     * @param string $code
     *
     * @return FB\Api\Data\FieldInterface
     * @throws Framework\Exception\NoSuchEntityException
     */
    public function getFieldByCode(string $code): FB\Api\Data\FieldInterface
    {
        foreach ($this->getFields() as $field) {
            if ($field->getCode() === $code) {
                return $field;
            }
        }

        throw new Framework\Exception\NoSuchEntityException(
            __('Unable to find a field with code "%1"', $code)
        );
    }

    public function getBackendModel(): FB\Api\Data\BackendModelInterface
    {
        return $this->backend;
    }

    public function getFrontendModel(): FB\Api\Data\FrontendModelInterface
    {
        return $this->frontend;
    }

    /**
     * @return FB\Api\Data\FieldInterface|null
     */
    public function getParentField(): ?FB\Api\Data\FieldInterface
    {
        return $this->parentField;
    }

    /**
     * @param FB\Api\Data\FieldInterface|null $parentField
     *
     * @return $this
     */
    public function setParentField(?FB\Api\Data\FieldInterface $parentField): FB\Fields\RepeaterType
    {
        $this->parentField = $parentField;

        return $this;
    }

    public function getValidatorConfig(): array
    {
        return [];
    }

    public function isRequired(): bool
    {
        return array_key_exists('required', $this->getValidatorConfig());
    }

    public function showRequiredMark(): bool
    {
        return $this->isRequired() === true
            && $this->isDisabled() === false
            && $this->isReadOnly() === false;
    }

    /**
     * @return int|null
     */
    public function getActiveIteration(): ?int
    {
        return $this->activeIteration;
    }

    /**
     * @param int|null $activeIteration
     *
     * @return RepeaterType
     */
    public function setActiveIteration(?int $activeIteration): RepeaterType
    {
        $this->activeIteration = $activeIteration;

        return $this;
    }
}
