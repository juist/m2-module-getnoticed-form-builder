<?php

namespace GetNoticed\FormBuilder\Fields;

use GetNoticed\FormBuilder as FB;

class HiddenType implements
    FB\Api\Data\FieldInterface
{
    /**
     * @var FB\Fields\HiddenType\BackendModel
     */
    private $backend;

    /**
     * @var FB\Fields\HiddenType\FrontendModel
     */
    private $frontend;

    /**
     * @var FB\Api\Data\FieldInterface|null
     */
    private $parentField = null;

    /**
     * @var string
     */
    private $code;

    /**
     * @var string|null
     */
    private $label = null;

    public function __construct(
        FB\Fields\HiddenType\BackendModelFactory $backendFactory,
        FB\Fields\HiddenType\FrontendModelFactory $frontendFactory,
        string $code,
        ?string $label = null
    ) {
        $this->backend = $backendFactory->create();
        $this->frontend = $frontendFactory->create();
        $this->code = $code;
        $this->label = $label;
    }

    /**
     * @inheritdoc
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @inheritdoc
     */
    public function getName(): string
    {
        return $this->getCode();
    }

    /**
     * @inheritdoc
     */
    public function getId(): string
    {
        return $this->getCode();
    }

    /**
     * @return string|null
     */
    public function getLabel(): ?string
    {
        return $this->label;
    }

    /**
     * @inheritdoc
     */
    public function getBackendModel(): FB\Api\Data\BackendModelInterface
    {
        return $this->backend;
    }

    /**
     * @inheritdoc
     */
    public function getFrontendModel(): FB\Api\Data\FrontendModelInterface
    {
        return $this->frontend;
    }

    /**
     * @return FB\Api\Data\FieldInterface|null
     */
    public function getParentField(): ?FB\Api\Data\FieldInterface
    {
        return $this->parentField;
    }

    /**
     * @param FB\Api\Data\FieldInterface|null $parentField
     *
     * @return $this
     */
    public function setParentField(?FB\Api\Data\FieldInterface $parentField): FB\Fields\HiddenType
    {
        $this->parentField = $parentField;

        return $this;
    }
}
