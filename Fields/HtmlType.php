<?php

namespace GetNoticed\FormBuilder\Fields;

use GetNoticed\FormBuilder as FB;

class HtmlType implements
    FB\Api\Data\FieldInterface,
    FB\Api\Data\Field\VisibleFieldInterface,
    FB\Api\Data\Field\LabeledFieldInterface
{
    /**
     * @var FB\Fields\HtmlType\BackendModel
     */
    private $backend;

    /**
     * @var FB\Fields\HtmlType\FrontendModel
     */
    private $frontend;

    /**
     * @var FB\Api\Data\FieldInterface|null
     */
    private $parentField = null;

    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $label;

    /**
     * @var string|null
     */
    private $html;

    public function __construct(
        FB\Fields\HtmlType\BackendModelFactory $backendFactory,
        FB\Fields\HtmlType\FrontendModelFactory $frontendFactory,
        string $code,
        string $label,
        ?string $html = null
    ) {
        $this->backend = $backendFactory->create();
        $this->frontend = $frontendFactory->create();
        $this->code = $code;
        $this->label = $label;
        $this->html = $html;
    }

    /**
     * @inheritdoc
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @inheritdoc
     */
    public function getName(): string
    {
        return $this->getCode();
    }

    /**
     * @inheritdoc
     */
    public function getId(): string
    {
        return $this->getCode();
    }

    /**
     * @inheritdoc
     */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * @inheritdoc
     */
    public function getBackendModel(): FB\Api\Data\BackendModelInterface
    {
        return $this->backend;
    }

    /**
     * @inheritdoc
     */
    public function getFrontendModel(): FB\Api\Data\FrontendModelInterface
    {
        return $this->frontend;
    }

    /**
     * @return FB\Api\Data\FieldInterface|null
     */
    public function getParentField(): ?FB\Api\Data\FieldInterface
    {
        return $this->parentField;
    }

    /**
     * @param FB\Api\Data\FieldInterface|null $parentField
     *
     * @return $this
     */
    public function setParentField(?FB\Api\Data\FieldInterface $parentField): FB\Fields\HtmlType
    {
        $this->parentField = $parentField;

        return $this;
    }

    public function getHtml(): ?string
    {
        return $this->html;
    }
}
