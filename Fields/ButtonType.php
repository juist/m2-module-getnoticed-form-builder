<?php

namespace GetNoticed\FormBuilder\Fields;

use GetNoticed\FormBuilder as FB;

class ButtonType implements FB\Api\Data\FieldInterface, FB\Api\Data\Field\VisibleFieldInterface
{
    /**
     * @var FB\Fields\TextType\BackendModel
     */
    private $backend;

    /**
     * @var FB\Fields\TextType\FrontendModel
     */
    private $frontend;

    /**
     * @var FB\Api\Data\FieldInterface|null
     */
    private $parentField = null;

    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $label;

    /**
     * @var string
     */
    private $action;

    public function __construct(
        FB\Fields\ButtonType\BackendModelFactory $backendFactory,
        FB\Fields\ButtonType\FrontendModelFactory $frontendFactory,
        string $code,
        string $label,
        string $action
    ) {
        $this->backend = $backendFactory->create();
        $this->frontend = $frontendFactory->create();
        $this->code = $code;
        $this->label = $label;
        $this->action = $action;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getName(): string
    {
        return $this->getCode();
    }

    public function getId(): string
    {
        return $this->getCode();
    }

    public function getAction(): string
    {
        return $this->action;
    }
    
    public function getLabel(): string
    {
        return $this->label;
    }

    public function getBackendModel(): FB\Api\Data\BackendModelInterface
    {
        return $this->backend;
    }

    public function getFrontendModel(): FB\Api\Data\FrontendModelInterface
    {
        return $this->frontend;
    }

    /**
     * @return FB\Api\Data\FieldInterface|null
     */
    public function getParentField(): ?FB\Api\Data\FieldInterface
    {
        return $this->parentField;
    }

    /**
     * @param FB\Api\Data\FieldInterface|null $parentField
     *
     * @return $this
     */
    public function setParentField(?FB\Api\Data\FieldInterface $parentField): FB\Fields\ButtonType
    {
        $this->parentField = $parentField;

        return $this;
    }
}
