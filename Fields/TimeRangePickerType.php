<?php

namespace GetNoticed\FormBuilder\Fields;

use GetNoticed\FormBuilder as FB;

class TimeRangePickerType extends FB\Fields\TextType
{
    public $inputType = 'timerangepicker';
}
