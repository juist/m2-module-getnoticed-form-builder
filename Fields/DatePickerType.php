<?php

namespace GetNoticed\FormBuilder\Fields;

use GetNoticed\FormBuilder as FB;

class DatePickerType extends FB\Fields\TextType
{
    public $inputType = 'datepicker';
}
