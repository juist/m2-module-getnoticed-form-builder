<?php

namespace GetNoticed\FormBuilder\Fields\FileType;

use GetNoticed\FormBuilder as FB;

class FrontendModel extends FB\Fields\AbstractFrontendModel
{
    /**
     * @var FB\Api\Data\FormInterface
     */
    private $form;

    /**
     * @var FB\Api\Data\FieldInterface|FB\Fields\UploadType
     */
    private $field;

    /**
     * @var string
     */
    private $inputType = 'file';

    // DI

    /**
     * @var FB\Service\FormRenderer
     */
    private $formRenderer;

    /**
     * @inheritDoc
     */
    public function __construct(
        FB\Service\FormRenderer $formRenderer
    ) {
        $this->formRenderer = $formRenderer;
    }

    /**
     * @return string
     */
    public function getInputType(): string
    {
        return $this->inputType;
    }

    /**
     * @param FB\Api\Data\FormInterface  $form
     * @param FB\Api\Data\FieldInterface $field
     * @param array                      $post
     */
    public function getFieldValues(
        FB\Api\Data\FormInterface $form,
        FB\Api\Data\FieldInterface $field,
        array $post = []
    ): array {
        return [];
    }

    public function setFormField(FB\Api\Data\FormInterface $form, FB\Api\Data\FieldInterface $field)
    {
        $this->form = $form;
        $this->field = $field;
    }

    public function getRendererCode(): string
    {
        return 'file';
    }
}
