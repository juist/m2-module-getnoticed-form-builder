<?php

namespace GetNoticed\FormBuilder\Exception;

use Magento\Framework\Exception;

abstract class AbstractException extends Exception\LocalizedException
{
}
