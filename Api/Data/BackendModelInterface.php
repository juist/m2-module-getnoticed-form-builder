<?php

namespace GetNoticed\FormBuilder\Api\Data;

use Magento\Framework;
use GetNoticed\FormBuilder as FB;

interface BackendModelInterface
{
    /**
     * @param FB\Api\Data\FieldValidatorInterface ...$validators
     */
    public function addValidators(FB\Api\Data\FieldValidatorInterface ...$validators): void;

    /**
     * @param FB\Api\Data\FormInterface              $form
     * @param FB\Api\Data\FB\Api\Data\FieldInterface $field
     * @param int|array|double|string|null           $value
     */
    public function validate(
        FB\Api\Data\FormInterface $form,
        FB\Api\Data\FieldInterface $field,
        $value
    ): void;
}
