<?php

namespace GetNoticed\FormBuilder\Api\Data;

use GetNoticed\FormBuilder as FB;

interface EntryInterface
{
    const TABLE_NAME = 'getnoticed_fb_entries';
    const PRIMARY_KEY_FIELD = 'entry_id';

    const FIELD_FORM = 'form_code';
    const FIELD_CREATED_AT = 'created_at';
    const FIELD_ENTRY_DATA = 'entry_data';

    public function getId();

    public function getForm(): FB\Api\Data\FormInterface;

    public function setForm(FB\Api\Data\FormInterface $form): FB\Api\Data\EntryInterface;

    public function getCreatedAt(): \DateTime;

    public function setCreatedAt(\DateTime $createdAt): FB\Api\Data\EntryInterface;

    public function getEntryData(): array;

    public function setEntryData(array $data): FB\Api\Data\EntryInterface;
}
