<?php

namespace GetNoticed\FormBuilder\Api\Data;

use GetNoticed\FormBuilder as FB;

interface FrontendModelInterface
{
    /**
     * @param FormInterface  $form
     * @param FieldInterface $field
     * @return void
     */
    public function setFormField(FB\Api\Data\FormInterface $form, FB\Api\Data\FieldInterface $field);

    public function getRendererCode(): string;

    public function getFieldValues(
        FB\Api\Data\FormInterface $form,
        FB\Api\Data\FieldInterface $field,
        array $post = []
    ): array;
}
