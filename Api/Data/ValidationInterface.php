<?php

namespace GetNoticed\FormBuilder\Api\Data;

/**
 * Interface ValidationInterface
 * @package GetNoticed\FormBuilder\Api\Data
 */
interface ValidationInterface
{

    /**
     * @return string
     */
    public function getValidationClass(): string;

    /**
     * @return null|string
     */
    public function getValidationJsPath(): ?string;

}
