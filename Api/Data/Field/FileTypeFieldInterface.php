<?php

namespace GetNoticed\FormBuilder\Api\Data\Field;

use GetNoticed\FormBuilder\Api\Data\FieldInterface;

interface FileTypeFieldInterface extends FieldInterface
{
    /**
     * @return string[]
     */
    public function getAllowedFileTypeExtensions(): array;
}
