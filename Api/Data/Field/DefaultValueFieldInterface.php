<?php

namespace GetNoticed\FormBuilder\Api\Data\Field;

interface DefaultValueFieldInterface
{
    /**
     * Should return a format for the default, e.g. '{{ $customer.getName() }}' would be acceptable. 'Foobar' is
     * also an accepted default value. It will be processed before returning, so placeholders are replaced.
     *
     * @return string
     */
    public function getDefault(): ?string;
}
