<?php

namespace GetNoticed\FormBuilder\Api\Data\Field;

interface ValidatableFieldInterface
{
    public function getValidatorConfig(): array;

    public function isRequired(): bool;

    public function showRequiredMark(): bool;
}
