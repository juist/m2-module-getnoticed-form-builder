<?php

namespace GetNoticed\FormBuilder\Api\Data\Field;

interface DisableAbleInterface
{
    public function isDisabled(): bool;

    public function isReadOnly(): bool;
}
