<?php

namespace GetNoticed\FormBuilder\Api\Data\Field;

interface ConditionalFieldInterface
{

    /**
     * @return bool
     */
    public function hasConditions(): bool;

    /**
     * @return string
     */
    public function getConditionField(): ?\GetNoticed\FormBuilder\Api\Data\FieldInterface;

    /**
     * @return array|FB\Api\Data\ConditionInterface[]
     */
    public function getConditionValues(): array;

    /**
     * @return string
     */
    public function getInitialAction(): string;

}
