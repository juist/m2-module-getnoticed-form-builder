<?php

namespace GetNoticed\FormBuilder\Api\Data\Field;

interface LabeledFieldInterface
{
    public function getLabel(): string;
}
