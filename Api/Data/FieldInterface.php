<?php

namespace GetNoticed\FormBuilder\Api\Data;

use GetNoticed\FormBuilder as FB;

interface FieldInterface
{
    public function getCode(): string;

    public function getName(): string;

    public function getId(): string;

    public function getBackendModel(): FB\Api\Data\BackendModelInterface;

    public function getFrontendModel(): FB\Api\Data\FrontendModelInterface;

    public function getParentField(): ?FB\Api\Data\FieldInterface;

    public function setParentField(?FB\Api\Data\FieldInterface $parentField);
}
