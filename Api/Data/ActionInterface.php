<?php

namespace GetNoticed\FormBuilder\Api\Data;

use Magento\Framework;
use GetNoticed\FormBuilder as FB;

interface ActionInterface
{
    /**
     * Process the form and execute a specific action (e.g. create an entry in the database, send an e-mail, etc)
     *
     * If an error occurs during processing, an exception should be thrown.
     *
     * @param FB\Api\Data\FormInterface $form
     * @param array                     $post
     *
     * @throws Framework\Exception\LocalizedException
     */
    public function process(
        FB\Api\Data\FormInterface $form,
        array $post
    ): void;
}
