<?php

namespace GetNoticed\FormBuilder\Api\Data;

use Magento\Framework;
use GetNoticed\FormBuilder as FB;

interface FormInterface
{
    /**
     * @return string
     */
    public function getCode(): string;

    /**
     * @return FB\Api\Data\FieldInterface[]
     */
    public function getFields(): array;

    /**
     * @param string $postName
     *
     * @return FB\Api\Data\FieldInterface
     * @throws Framework\Exception\NoSuchEntityException
     */
    public function getFieldByPostName(string $postName): FB\Api\Data\FieldInterface;

    /**
     * @param FB\Api\Data\FieldInterface[] $fields
     *
     * @return FB\Api\Data\FormInterface
     */
    public function setFields(array $fields): FB\Api\Data\FormInterface;

    /**
     * @return FB\Api\Data\ActionInterface[]
     */
    public function getActions(): array;

    /**
     * @return FB\Api\Data\ValidationInterface[]
     */
    public function getCustomValidations(): array;

    /**
     * @param FB\Api\Data\ActionInterface[] $actions
     *
     * @return FB\Api\Data\FormInterface
     */
    public function setActions(array $actions): FB\Api\Data\FormInterface;

    /**
     * @return array
     */
    public function getConfig(): array;

    /**
     * @param string                             $key
     * @param int|float|double|string|array|null $default
     *
     * @return int|float|double|string|array|null
     */
    public function getConfigItem(string $key, $default = null);

    /**
     * @return string
     */
    public function getNameContainer(): string;

    /**
     * @return string
     */
    public function getFormId(): string;

    /**
     * @param array $config
     *
     * @return FB\Api\Data\FormInterface
     */
    public function setConfig(array $config): FB\Api\Data\FormInterface;

    /**
     * @param \GetNoticed\FormBuilder\Api\Data\FieldInterface $field
     * @param FB\Api\Data\FieldInterface|null                 $parentField
     * @param int|null                                        $iteration
     *
     * @return string|int|float|double|array|null
     */
    public function getFieldValue(
        FB\Api\Data\FieldInterface $field,
        ?FB\Api\Data\FieldInterface $parentField = null,
        ?int $iteration = null
    );

    /**
     * @return array
     */
    public function getValues(): array;

    /**
     * @param array $values
     *
     * @return \GetNoticed\FormBuilder\Api\Data\FormInterface
     */
    public function setValues(array $values): FB\Api\Data\FormInterface;

    /**
     * @return string
     */
    public function getRedirectUrl(): string;

    /**
     * @return bool
     */
    public function isSendSuccessMessage(): bool;

    /**
     * @return \GetNoticed\FormBuilder\Api\FieldInterface[]
     */
    public function getFileTypeFields(): array;
}
