<?php

namespace GetNoticed\FormBuilder\Api\Data;

use Magento\Framework;
use GetNoticed\FormBuilder as FB;

interface SelectOptionsInterface extends Framework\Option\ArrayInterface
{
    public function setEntities(
        FB\Api\Data\FormInterface $form,
        FB\Api\Data\FieldInterface $field,
        ?FB\Api\Data\FieldInterface $parentField = null,
        ?int $iteration = null
    ): void;
}
