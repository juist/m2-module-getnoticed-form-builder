<?php

namespace GetNoticed\FormBuilder\Api\Data;

use Magento\Framework;
use GetNoticed\FormBuilder as FB;

interface FieldValidatorInterface
{
    /**
     * @param FB\Api\Data\FormInterface     $form
     * @param FB\Api\Data\FieldInterface    $field
     * @param int|array|double|string|null $value
     *
     * @throws Framework\Exception\LocalizedException
     */
    public function validateField(
        FB\Api\Data\FormInterface $form,
        FB\Api\Data\FieldInterface $field,
        $value
    ): void;

    public function getConfig(): array;

    public function setConfig(array $config): FB\Api\Data\FieldValidatorInterface;
}
