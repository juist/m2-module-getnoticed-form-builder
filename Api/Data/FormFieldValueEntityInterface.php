<?php

namespace GetNoticed\FormBuilder\Api\Data;

interface FormFieldValueEntityInterface
{
    /**
     * @return object
     */
    public function getEntity();
}
