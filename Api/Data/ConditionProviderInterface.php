<?php

namespace GetNoticed\FormBuilder\Api\Data;

interface ConditionProviderInterface
{

    /**
     * @return ConditionInterface[]
     */
    public function getConditions(): array;

}
