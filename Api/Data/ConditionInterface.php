<?php

namespace GetNoticed\FormBuilder\Api\Data;

interface ConditionInterface
{

    public const ALLOWED_ACTIONS = ['show', 'hide'];

    /**
     * can be any scalar type
     * @return mixed
     */
    public function getValue();

    /**
     * Should only return one of "show" or "hide"
     * @return string
     */
    public function getAction(): string;

}
