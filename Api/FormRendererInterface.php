<?php

namespace GetNoticed\FormBuilder\Api;

use Magento\Framework;
use GetNoticed\FormBuilder as FB;

interface FormRendererInterface
{
    public function render(FB\Api\Data\FormInterface $form): string;

    /**
     * @return Framework\View\Element\RendererList|Framework\View\Element\BlockInterface
     */
    public function getFieldsRendererList(): Framework\View\Element\RendererList;

    /**
     * @return Framework\View\Element\RendererList|Framework\View\Element\BlockInterface
     */
    public function getFormsRendererList(): Framework\View\Element\RendererList;
}
