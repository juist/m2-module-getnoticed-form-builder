<?php

namespace GetNoticed\FormBuilder\Api;

use GetNoticed\FormBuilder\Api\Data\FormInterface;

interface FormRepositoryInterface
{

    public function getFormByCode(string $code): FormInterface;

}
