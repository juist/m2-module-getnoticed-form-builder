<?php

namespace GetNoticed\FormBuilder\Api;

use Magento\Framework;
use GetNoticed\FormBuilder as FB;

interface EntryRepositoryInterface
{
    /**
     * @param int $entryId
     *
     * @return FB\Api\Data\EntryInterface
     * @throws Framework\Exception\NoSuchEntityException
     */
    public function get(int $entryId): FB\Api\Data\EntryInterface;

    /**
     * @param FB\Api\Data\EntryInterface|FB\Model\Entry $entry
     *
     * @return FB\Api\Data\EntryInterface|FB\Model\Entry
     * @throws \Exception
     */
    public function save(FB\Api\Data\EntryInterface $entry): FB\Api\Data\EntryInterface;
}
