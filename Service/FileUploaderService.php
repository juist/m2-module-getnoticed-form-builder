<?php

namespace GetNoticed\FormBuilder\Service;

use GetNoticed\FormBuilder\Api\Data\Field\FileTypeFieldInterface;
use GetNoticed\FormBuilder\Api\Data\FieldInterface;
use GetNoticed\FormBuilder\Api\FormRepositoryInterface;
use Lof\Formbuilder\Block\Field;
use Magento\Framework\Exception\LocalizedException;
use Magento\MediaStorage\Model\File\UploaderFactory;
use Magento\Framework\App\Filesystem\DirectoryList;
use \Magento\Framework\App\RequestInterface;
use Magento\Framework\Filesystem\Io\File as Io;

class FileUploaderService
{
    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var UploaderFactory
     */
    private $fileUploaderFactory;

    /**
     * @var DirectoryList
     */
    private $directoryList;

    /**
     * @var FormRepositoryInterface
     */
    private $formRepository;

    public function __construct(
        RequestInterface $request,
        UploaderFactory $fileUploaderFactory,
        DirectoryList $directoryList,
        FormRepositoryInterface $formRepository,
        Io $io
    ) {
        $this->request = $request;
        $this->fileUploaderFactory = $fileUploaderFactory;
        $this->directoryList = $directoryList;
        $this->formRepository = $formRepository;
        $this->io = $io;
    }

    /**
     * @throws LocalizedException
     * @throws \Magento\Framework\Exception\FileSystemException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function processUpload()
    {
        $field = $this->getFileUploadField();

        $path = $this->directoryList->getPath(DirectoryList::MEDIA) . DIRECTORY_SEPARATOR . 'formbuilder' . DIRECTORY_SEPARATOR;
        $this->io->checkAndCreateFolder($path);

        $uploader = $this->fileUploaderFactory->create(['fileId' => $field->getName()]);
        $uploader->setAllowedExtensions($field->getAllowedFileTypeExtensions());
        $uploader->setAllowRenameFiles(true);
        return $uploader->save($path);
    }

    /**
     * @return FileTypeFieldInterface
     * @throws LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    private function getFileUploadField(): FileTypeFieldInterface
    {
        $form = $this->formRepository->getFormByCode($this->getRequest()->getParam('form'));
        $field = $form->getFieldByPostName($this->getRequest()->getParam('field'));
        if (!$field instanceof FileTypeFieldInterface) {
            throw new LocalizedException(__('invalid field'));
        }
        return $field;
    }

    /**
     * @return RequestInterface
     */
    private function getRequest(): RequestInterface
    {
        return $this->request;
    }

}
