<?php

namespace GetNoticed\FormBuilder\Service;

class ConditionService {

    /**
     * Returns true when conditions are applied and field is shown.
     * @param $field
     * @param $value
     * @return bool
     */
    public function checkConditions($field, $value): bool {
        if (
            $field instanceof FB\Api\Data\Field\ConditionalFieldInterface &&
            $field->hasConditions()
        ) {
            foreach ($field->getConditionValues() as $condition) {
                if ($form->getFieldValue($field->getConditionField()) === $condition->getValue() && $condition->getAction() === 'show') {
                    return true;
                }
            }
        }
        return false;
    }

}
