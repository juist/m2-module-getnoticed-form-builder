<?php

namespace GetNoticed\FormBuilder\Service;

use GetNoticed\Common\Helper\StringHelper;
use Magento\Customer;

class FormSessionPersistenceService
{
    /**
     * @var Customer\Model\Session
     */
    private $customerSession;

    // DI

    /**
     * @var Customer\Model\SessionFactory
     */
    private $customerSessionFactory;

    public function __construct(
        Customer\Model\SessionFactory $customerSessionFactory
    ) {
        $this->customerSessionFactory = $customerSessionFactory;
    }

    public function fetchFormData(string $formId, $removeFromSession = false): array
    {
        $getMethod = $this->getSessionDataCamelCaseMethod('get', $formId);
        $unsMethod = $this->getSessionDataCamelCaseMethod('uns', $formId);

        $data = $this->getCustomerSession()->{$getMethod}();

        if ($removeFromSession) {
            $this->getCustomerSession()->{$unsMethod}();
        }

        return $data ?: [];
    }

    public function removeFormData(string $formId): void
    {
        $unsMethod = $this->getSessionDataCamelCaseMethod('uns', $formId);
        $this->getCustomerSession()->{$unsMethod}();
    }

    public function saveFormData(string $formId, array $formData): void
    {
        $setMethod = $this->getSessionDataCamelCaseMethod('set', $formId);

        $this->getCustomerSession()->{$setMethod}($formData);
    }

    private function getCustomerSession(): Customer\Model\Session
    {
        if ($this->customerSession instanceof Customer\Model\Session) {
            return $this->customerSession;
        }

        return $this->customerSession = $this->customerSessionFactory->create();
    }

    private function getSessionDataCamelCaseMethod(string $methodPrefix, string $formId): string
    {
        return sprintf('%sGetNoticedFormBuilderData%s', $methodPrefix, StringHelper::camelCase($formId));
    }
}
