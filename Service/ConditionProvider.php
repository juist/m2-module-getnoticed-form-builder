<?php

namespace GetNoticed\FormBuilder\Service;

use GetNoticed\FormBuilder\Api\Data\ConditionInterface;
use GetNoticed\FormBuilder\Api\Data\ConditionInterfaceFactory;
use Magento\Framework\Exception\LocalizedException;
use GetNoticed\FormBuilder\Api\Data\ConditionProviderInterface;

class ConditionProvider implements ConditionProviderInterface
{

    /**
     * @var ConditionInterface[]|null
     */
    private $conditions = null;

    /**
     * @var array
     */
    protected $conditionValues = [];

    /**
     * @var ConditionInterfaceFactory
     */
    private $conditionFactory;

    /**
     * ConditionProvider constructor.
     * @param array $conditionValues
     */
    public function __construct(
        ConditionInterfaceFactory $conditionFactory,
        array $conditionValues
    ) {
        $this->conditionFactory = $conditionFactory;
        $this->conditionValues = $conditionValues;
    }

    /**
     * @return ConditionInterface[]
     */
    public function getConditions(): array
    {
        if ($this->conditions !== null) {
            return $this->conditions;
        }
        foreach ($this->conditionValues as $conditionValue) {
            try {
                $this->conditions[] = $this->conditionFactory->create([
                    'value' => $conditionValue['value'],
                    'action' => $conditionValue['action']
                ]);
            } catch (LocalizedException $e) {
                continue;
            }
        }
        return $this->conditions;
    }

}
