<?php

namespace GetNoticed\FormBuilder\Service;

use Magento\Framework;
use GetNoticed\FormBuilder as FB;

class FormRenderer implements FB\Api\FormRendererInterface
{
    /**
     * @var FB\Service\FormSessionPersistenceService
     */
    private $formSessionPersistenceService;

    /**
     * @var Framework\View\LayoutInterface
     */
    private $layout;

    public function __construct(
        FB\Service\FormSessionPersistenceService $formSessionPersistenceService,
        Framework\View\LayoutInterface $layout
    ) {
        $this->formSessionPersistenceService = $formSessionPersistenceService;
        $this->layout = $layout;
    }

    public function render(FB\Api\Data\FormInterface $form): string
    {
        $formHtml = $this->getFormsRendererList()->getRenderer('default')
                         ->setData('form', $form)
                         ->toHtml();

        $this->formSessionPersistenceService->removeFormData($form->getCode());

        return $formHtml;
    }

    /**
     * @return \Magento\Framework\View\Element\RendererList|\Magento\Framework\View\Element\BlockInterface
     */
    public function getFieldsRendererList(): Framework\View\Element\RendererList
    {
        return $this->layout->getBlock('fields.renderers');
    }

    /**
     * @return \Magento\Framework\View\Element\RendererList|\Magento\Framework\View\Element\BlockInterface
     */
    public function getFormsRendererList(): Framework\View\Element\RendererList
    {
        return $this->layout->getBlock('form.renderers');
    }
}
