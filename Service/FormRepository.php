<?php

namespace GetNoticed\FormBuilder\Service;

use GetNoticed\FormBuilder\Api\Data\FormInterface;
use GetNoticed\FormBuilder\Api\FormRepositoryInterface;
use Magento\Framework\Exception\LocalizedException;

class FormRepository implements FormRepositoryInterface
{

    /**
     * FormRepository constructor.
     * @param FormInterface[] $forms
     */
    public function __construct(
        array $forms
    ) {
        foreach ($forms as $form) {
            if (!$form instanceof FormInterface) {
                throw new LocalizedException(__('invalid form'));
            }
        }
        $this->forms = $forms;
    }

    public function getFormByCode(string $code): FormInterface
    {
        $result = array_filter($this->forms, function (FormInterface $form) use ($code) {
            return $form->getCode() === $code;
        });
        return array_shift($result);
    }

}
