<?php

namespace GetNoticed\FormBuilder\Service;

use Magento\Framework\File\Mime;
use Magento\Framework\Filesystem\Driver\File;
use Magento\Framework\Filesystem\Io\File as Io;

class AttachmentFactory
{

    /**
     * @var File
     */
    private $driver;

    public function __construct(
        File $driver,
        Mime $mime,
        Io $io
    ) {
        $this->driver = $driver;
        $this->mime = $mime;
        $this->io = $io;
    }

    /**
     * Return array in following format:
     * [
     *      'content',
            \Zend_Mime::TYPE_OCTETSTREAM,
            \Zend_Mime::DISPOSITION_ATTACHMENT,
            \Zend_Mime::ENCODING_BASE64,
            'filename.txt'
     * ]
     * @return array
     */
    public function createAttachment(string $filePath): array
    {
        return [
            $this->driver->fileGetContents($filePath),
            $this->mime->getMimeType($filePath),
            \Zend_Mime::DISPOSITION_ATTACHMENT,
            \Zend_Mime::ENCODING_BASE64,
            $this->io->getPathInfo($filePath)['basename']
        ];
    }

}
