<?php

namespace GetNoticed\FormBuilder\Service;

use GetNoticed\FormBuilder as FB;

class FormFieldValueProcessor
{
    const METHOD_CALL_PLACEHOLDER_REGEX =
        '#({{)(\ )?(\$)?(?P<entity>[a-zA-Z0-9].*)(\.)(?P<data_source>[a-zA-Z0-9].*)(\ )?(}})#U';
    const IS_METHOD_REGEX = '#^(.*)(\(\))$#';

    /**
     * @var FB\Api\Data\FormFieldValueEntityInterface[]
     */
    private $entities = [];

    public function __construct(
        array $entities = []
    ) {
        $this->entities = $entities;
    }

    /**
     * @return \GetNoticed\FormBuilder\Api\Data\FormFieldValueEntityInterface[]
     */
    public function getEntities(): array
    {
        return $this->entities;
    }

    public function processFieldValue(?string $value): string
    {
        if ($value === null) {
            return '';
        }

        return $this->replaceMethodCallPlaceholders($value);
    }

    public function replaceMethodCallPlaceholders(string $value): string
    {
        return preg_replace_callback(self::METHOD_CALL_PLACEHOLDER_REGEX, [$this, 'replacePlaceholder'], $value);
    }

    private function replacePlaceholder(array $match): string
    {
        // Get variables (value, entity key and data source)
        $entityKey = $match['entity'];
        $dataSource = $match['data_source'];

        // Get entity model
        if (array_key_exists($entityKey, $this->entities) !== true) {
            return '';
        }

        $entity = $this->entities[$entityKey]->getEntity();

        // Method or variable?
        $isMethod = preg_match(self::IS_METHOD_REGEX, $dataSource) === 1;

        if ($isMethod) {
            $method = strtr($dataSource, ['()' => '']);

            return method_exists($entity, $method) || is_callable([$entity, $method]) ? $entity->{$method}() ?: '' : '';
        } else {
            $vars = get_object_vars($entity);

            return array_key_exists($dataSource, $vars) === true ? $vars[$dataSource] ?: '' : '';
        }
    }
}
