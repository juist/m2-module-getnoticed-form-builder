<?php

namespace GetNoticed\FormBuilder\Service;

use Magento\Framework;
use GetNoticed\FormBuilder as FB;

class FormValidationService
{
    /**
     * @var FB\Api\Data\FieldValidatorInterface[]
     */
    private $validators = [];

    public function __construct(
        array $validators = []
    ) {
        $this->validators = $validators;
    }

    /**
     * @param \GetNoticed\FormBuilder\Api\Data\FormInterface $form
     * @param array                                          $post
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function validateForm(
        FB\Api\Data\FormInterface $form,
        array $post
    ): void {
        $form->setValues($post);

        foreach ($form->getFields() as $field) {
            $this->validateField($form, $field);
        }
    }

    /**
     * @param FB\Api\Data\FormInterface          $form
     * @param FB\Api\Data\FieldInterface         $field
     * @param int|array|double|float|string|null $value
     *
     * @throws Framework\Exception\LocalizedException
     */
    public function validateField(
        FB\Api\Data\FormInterface $form,
        FB\Api\Data\FieldInterface $field,
        $value = null
    ): void {
        if ($field instanceof FB\Api\Data\Field\ValidatableFieldInterface) {
            $field->getBackendModel()->addValidators(...$this->generateValidators($field->getValidatorConfig()));
            $field->getBackendModel()->validate($form, $field, $value ?: $form->getFieldValue($field));
        }
    }

    /**
     * @param array $validatorConfig
     *
     * @return array
     * @throws Framework\Exception\LocalizedException
     */
    private function generateValidators(array $validatorConfig): array
    {
        $validators = [];

        foreach ($validatorConfig as $code => $settings) {
            $validators[] = $this->getValidator($code, $settings);
        }

        return $validators;
    }

    /**
     * @param string $validatorCode
     * @param array  $validatorSettings
     *
     * @return FB\Api\Data\FieldValidatorInterface
     * @throws Framework\Exception\LocalizedException
     */
    private function getValidator(string $validatorCode, array $validatorSettings = [])
    {
        if (array_key_exists($validatorCode, $this->validators) !== true) {
            throw new Framework\Exception\LocalizedException(
                __('No validator with code "%1" found', $validatorCode)
            );
        }

        return $this->validators[$validatorCode]->setConfig($validatorSettings);
    }
}