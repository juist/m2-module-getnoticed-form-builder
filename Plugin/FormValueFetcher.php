<?php

namespace GetNoticed\FormBuilder\Plugin;

use GetNoticed\FormBuilder as FB;

class FormValueFetcher
{
    /**
     * @var FB\Model\Data\AbstractForm
     */
    private $form;

    /**
     * @var FB\Api\Data\FieldInterface
     */
    private $field;

    /**
     * @var array
     */
    private $formData;

    // DI

    /**
     * @var FB\Service\FormSessionPersistenceService
     */
    private $formSessionPersistenceService;

    public function __construct(
        FB\Service\FormSessionPersistenceService $formSessionPersistenceService
    ) {
        $this->formSessionPersistenceService = $formSessionPersistenceService;
    }

    public function aroundGetFieldValue(
        FB\Model\Data\AbstractForm $form,
        callable $next,
        FB\Api\Data\FieldInterface $field,
        ?FB\Api\Data\FieldInterface $parentField = null,
        ?int $iteration = null
    ) {
        $this->form = $form;
        $this->field = $field;

        if ($iteration !== null && $parentField !== null && $parentField instanceof FB\Fields\RepeaterType) {
            $data = array_values($form->getFieldValue($parentField));
            $default = array_values(
                array_key_exists($parentField->getName(), $form->getDefaultValues())
                    ? $form->getDefaultValues()[$parentField->getName()] : []
            );

            if ($field instanceof FB\Api\Data\Field\DisableAbleInterface
                && ($field->isDisabled() || $field->isReadOnly())) {
                return $this->getRepeaterValue($default, $iteration, $field);
            } else {
                return $this->getRepeaterValue(
                    $data,
                    $iteration,
                    $field,
                    $this->getRepeaterValue($default, $iteration, $field)
                );
            }
        }

        return $this->getValue($field->getName()) ?: $next($field, $parentField, $iteration);
    }

    private function getValue(string $key)
    {
        if ($this->formData === null) {
            $this->formData = $this->formSessionPersistenceService->fetchFormData($this->form->getCode());
        }

        return array_key_exists($key, $this->formData) ? $this->formData[$key] : null;
    }

    private function getRepeaterValue(
        array $values,
        int $iteration,
        FB\Api\Data\FieldInterface $field,
        $default = null
    ) {
        return array_key_exists($iteration, $values) && array_key_exists($field->getName(), $values[$iteration])
            ? $values[$iteration][$field->getName()] : $default;
    }
}
