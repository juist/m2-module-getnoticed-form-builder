<?php

namespace GetNoticed\FormBuilder\Plugin;

use Magento\Framework;
use GetNoticed\FormBuilder as FB;

class FormDefaultValues
{
    /**
     * @var FB\Model\Data\AbstractForm
     */
    private $form;

    /**
     * @var FB\Api\Data\FieldInterface
     */
    private $field;

    // DI

    /**
     * @var FB\Service\FormFieldValueProcessor
     */
    private $formFieldValueProcessor;

    /**
     * @var Framework\App\RequestInterface|Framework\App\Request\Http
     */
    private $request;

    public function __construct(
        FB\Service\FormFieldValueProcessor $formFieldValueProcessor,
        Framework\App\RequestInterface $request
    ) {
        $this->formFieldValueProcessor = $formFieldValueProcessor;
        $this->request = $request;
    }

    public function afterGetFieldValue(
        FB\Model\Data\AbstractForm $form,
        $value,
        FB\Api\Data\FieldInterface $field,
        ?FB\Api\Data\FieldInterface $parentField = null,
        ?int $iteration = null
    ) {
        $this->form = $form;
        $this->field = $field;

        if ($this->request->getMethod() !== 'GET') {
            // Prevent the replacing of empty post values (creates unexpected behaviour for end users)
            return $value;
        }

        if (empty($value) && $field instanceof FB\Api\Data\Field\DefaultValueFieldInterface) {
            $value = $this->formFieldValueProcessor->processFieldValue($field->getDefault());
        }

        return $value;
    }
}
