<?php

namespace GetNoticed\FormBuilder\Plugin;

use Magento\Framework;
use GetNoticed\FormBuilder as FB;

class Entry
{
    /**
     * @var Framework\Serialize\Serializer\Json
     */
    private $serializer;

    /**
     * @var FB\Model\Data\FormFactory
     */
    private $formFactory;

    public function __construct(
        Framework\Serialize\Serializer\Json $serializer,
        FB\Model\Data\FormFactory $formFactory
    ) {
        $this->serializer = $serializer;
        $this->formFactory = $formFactory;
    }

    public function beforeBeforeSave(
        FB\Model\Entry $entry
    ) {
        $entry->setData(FB\Api\Data\EntryInterface::FIELD_FORM, $entry->getForm()->getCode());
        $entry->setData(
            FB\Api\Data\EntryInterface::FIELD_ENTRY_DATA,
            $this->serializer->serialize($entry->getEntryData())
        );

        if ($entry->isObjectNew()) {
            $entry->setCreatedAt(new \DateTime());
        }

        return $entry;
    }

    public function afterAfterLoad(
        FB\Model\Entry $entry
    ) {
        $entry->setForm(
            $this->formFactory->create(
                FB\Api\Data\FormInterface::class,
                ['form_code' => $entry->getData(FB\Api\Data\EntryInterface::FIELD_FORM)]
            )
        );
        $entry->setEntryData(
            $this->serializer->unserialize($entry->getData(FB\Api\Data\EntryInterface::FIELD_ENTRY_DATA))
        );

        return $entry;
    }
}
