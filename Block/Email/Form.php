<?php

namespace GetNoticed\FormBuilder\Block\Email;

use Magento\Framework;
use GetNoticed\FormBuilder as FB;
use Magento\Framework\DataObject;

class Form implements Framework\View\Element\Block\ArgumentInterface
{
    private $block;

    public function __construct(dataObject $data)
    {
        $this->_data = $data;
    }
    /**
     * @var Framework\View\Element\Template
     */

    /**
     * @return FB\Api\Data\FormInterface
     */
    public function getForm(): FB\Api\Data\FormInterface
    {
        $form = $this->_data->getData('form');
        if(empty($form)) {
            return  $this->getData()['form'];
        }
        return $form;
    }

    public function getData() {
        return $this->_data->getData();
    }

    /**
     * @return array
     */
    public function getPost(): array
    {
        $post = $this->_data->getData('post');
        if(empty($post)) {
            return  $this->getData()['post'];
        }
        return $post;
    }

    /**
     * @param Framework\View\Element\Template $block
     *
     * @return FB\Block\Email\Form
     */
    public function setBlock(Framework\View\Element\Template $block): FB\Block\Email\Form
    {
        $this->block = $block;

        return $this;
    }

    /**
     * @return array
     */
    public function getFieldValues(): array
    {
        $values = [];

        foreach ($this->getFields($this->getForm()) as $field) {
            $values += $field->getFrontendModel()->getFieldValues($this->getForm(), $field, $this->getPost());
        }

        return $values;
    }

    /**
     * @return array
     */
    public function getEmailValues(): array
    {
        return $this->filterRemoveEmptyValues($this->getFieldValues());
    }

    /**
     * Removes all empty values from array, even from nested arrays.
     *
     * @param array $data
     *
     * @return array
     */
    function filterRemoveEmptyValues(array $data)
    {
        foreach ($data as $key => $value) {
            if (is_array($value) && !array_key_exists('value', $value)) {
                unset($data[$key]);
                continue;
            }
            if (is_array($value) && array_key_exists('value', $value) && is_array($value['value'])) {
                $data[$key]['value'] = $this->filterRemoveEmptyValues($value['value']);
                if (isset($value['label'])) {
                    if ($value['label'] !== '') {
                        $data[$key]['label'] = $value['label'];
                    } else if ($value['value'] === '') {
                        unset($data[$key]);
                    }
                }  else {
                    unset($data[$key]);
                }
            } elseif (is_array($value)) {
                if (isset($value['value'])) {
                    if ($value['value'] === '' || $value['value'] === 0 || strlen($value['value']) < 1) {
                        unset($data[$key]);
                    }
                } else {
                    unset($data[$key]);
                }
            }
        }
        return $data;
    }

    /**
     * @param FB\Api\Data\FormInterface $form
     *
     * @return FB\Api\Data\FieldInterface[]
     */
    public function getFields(FB\Api\Data\FormInterface $form): array
    {
        return $form->getFields();
    }

    /**
     * @param FB\Api\Data\FormInterface $form
     *
     * @return FB\Api\Data\FieldInterface[]
     */
    private function getVisibleFields(FB\Api\Data\FormInterface $form): array
    {
        $fields = [];

        foreach ($form->getFields() as $field) {
            if ($field instanceof FB\Api\Data\Field\VisibleFieldInterface) {
                $fields[] = $field;
            }
        }

        return $fields;
    }

    /**
     * @param FB\Api\Data\FieldInterface $field
     *
     * @return string|null
     */
    private function getFieldLabel(FB\Api\Data\FieldInterface $field): ?string
    {
        if ($field instanceof FB\Api\Data\Field\LabeledFieldInterface) {
            return $field->getLabel();
        }

        return null;
    }

    /**
     * @param FB\Api\Data\FormInterface  $form
     * @param FB\Api\Data\FieldInterface $field
     *
     * @return array|float|int|string|null
     */
    private function getFieldValue(
        FB\Api\Data\FormInterface $form,
        FB\Api\Data\FieldInterface $field
    ) {
        return $form->getFieldValue($field);
    }
}
