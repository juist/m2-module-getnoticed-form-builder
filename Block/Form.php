<?php

namespace GetNoticed\FormBuilder\Block;

use Magento\Customer\Model\Session;
use Magento\Framework;
use GetNoticed\FormBuilder as FB;
use Magento\Framework\App\Response\Http;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\View\Element\BlockInterface;

class Form implements Framework\View\Element\Block\ArgumentInterface
{
    /**
     * @var FB\Service\FormRenderer
     */
    private $formRenderer;

    /**
     * @var Session
     */
    private $customerSession;

    /**
     * @var Http
     */
    private $redirect;

    /**
     * @var ManagerInterface
     */
    private $messageManager;

    public function __construct(
        FB\Service\FormRenderer $formRenderer,
        Session $customerSession,
        Http $redirect,
        ManagerInterface $messageManager,
    ) {
        $this->formRenderer = $formRenderer;
        $this->customerSession = $customerSession;
        $this->redirect = $redirect;
        $this->messageManager = $messageManager;

    }

    public function renderFieldHtml(
        FB\Api\Data\FormInterface $form,
        FB\Api\Data\FieldInterface $field
    ): string {
        $field->getFrontendModel()->setFormField($form, $field);
        $renderer = $this->formRenderer->getFieldsRendererList()
                                       ->getRenderer(
                                           $field->getFrontendModel()->getRendererCode(),
                                           null,
                                           $field->getFrontendModel()->getParam('template')
                                       );
        $renderer->setData('form', $form)->setData('field', $field);

        return $renderer->toHtml();
    }

    public function isFieldVisible(
        FB\Api\Data\FieldInterface $field
    ): bool {
        return FB\Helper\FormFieldHelper::isVisible($field);
    }

    /**
     * @param \GetNoticed\FormBuilder\Api\Data\FieldInterface|FB\Api\Data\Field\LabeledFieldInterface $field
     *
     * @return null|string
     */
    public function getFieldLabel(
        FB\Api\Data\FieldInterface $field
    ): ?string {
        return $field instanceof FB\Api\Data\Field\LabeledFieldInterface ? __($field->getLabel()) : null;
    }

    /**
     * @param FB\Api\Data\FieldInterface $field
     * @return bool
     */
    public function showFieldLabel(
        FB\Api\Data\FieldInterface $field
    ): bool {
        return $this->getFieldLabel($field) ? true : false;
    }

    public function getFieldId(
        FB\Api\Data\FormInterface $form,
        FB\Api\Data\FieldInterface $field
    ): string {
        return FB\Helper\FormFieldHelper::getFieldId($form, $field);
    }

    public function showRequiredMark(FB\Api\Data\FieldInterface $field): bool
    {
        return $field instanceof FB\Api\Data\Field\ValidatableFieldInterface ? $field->showRequiredMark() : false;
    }

    public function checkIfLogggedIn($check = false) {
        $loggedin = $this->customerSession->isLoggedIn();
        $data = $this->customerSession->getData();

        if (isset($data['customer_group_id'])) {
            $group_id = $data['customer_group_id'];
        } else {
            $group_id = 0;
        }

        if (!$loggedin) {
            if ($group_id == 0) {
                if ($check) {
                    return false;
                }
                $this->redirect->setRedirect('/customer/account/login');
                $this->messageManager->addErrorMessage(__("Please login to fill in this form"));
            } else {
                return true;
            }
        } else {
            return true;
        }
    }
}
