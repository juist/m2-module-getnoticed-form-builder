<?php

namespace GetNoticed\FormBuilder\Block;

use Magento\Framework;
use GetNoticed\FormBuilder as FB;

class FormField implements Framework\View\Element\Block\ArgumentInterface
{
    public function getFieldName(
        FB\Api\Data\FormInterface $form,
        FB\Api\Data\FieldInterface $field,
        ?string $nameSuffix = null
    ): string {
        return FB\Helper\FormFieldHelper::getFieldName($form, $field, $nameSuffix);
    }

    public function getFieldId(
        FB\Api\Data\FormInterface $form,
        FB\Api\Data\FieldInterface $field,
        ?string $nameSuffix = null
    ): string {
        return FB\Helper\FormFieldHelper::getFieldId($form, $field, $nameSuffix);
    }

    public function getFieldLabel(
        FB\Api\Data\FieldInterface $field
    ): ?string {
        return $field instanceof FB\Api\Data\Field\LabeledFieldInterface ? __($field->getLabel()) : null;
    }
}
