<?php

namespace GetNoticed\FormBuilder\Block;

use Magento\Framework;
use GetNoticed\FormBuilder as FB;

class FormBlock implements Framework\View\Element\Block\ArgumentInterface
{
    /**
     * @var FB\Api\FormRendererInterface
     */
    private $formRenderer;

    public function __construct(
        FB\Api\FormRendererInterface $formRenderer
    ) {
        $this->formRenderer = $formRenderer;
    }

    public function render(FB\Api\Data\FormInterface $form): string
    {
        return $this->formRenderer->render($form);
    }
}
