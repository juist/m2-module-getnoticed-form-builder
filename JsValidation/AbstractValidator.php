<?php

namespace GetNoticed\FormBuilder\JsValidation;

use GetNoticed\FormBuilder\Api\Data\ValidationInterface;

class AbstractValidator implements ValidationInterface
{

    public function __construct(
        string $validationClass,
        ?string $validationJsPath = null
    ) {
        $this->validationClass = $validationClass;
        $this->validationJsPath = $validationJsPath;
    }

    public function getValidationClass(): string
    {
        return $this->validationClass;
    }

    public function getValidationJsPath(): ?string
    {
        return $this->validationJsPath;
    }

}
